package com.j2core.elozovan.week04.zoopark;

import com.j2core.elozovan.week04.zoopark.animals.*;

import java.util.Random;
import java.util.UUID;

/**
 * Creates animals.
 */
public class AnimalFactory {

    public static Animal create(){
        String name = "Zvr-" + UUID.randomUUID().toString();
        return create(name);
    }

    /**
     * Creates an animal of some kind.
     *
     * @param nameForAnimal - could be null. This is just a crutch to overcome "no default parameter values" in Java.
     * @return - an animal created.
     */
    public static Animal create(String nameForAnimal){
        int flippedCoin = new Random().nextInt(10);

        switch (flippedCoin) {
            case 0:
            case 7:
            default:
                return new Bear(nameForAnimal);
            case 1:
            case 8:
                return new Lion(nameForAnimal);
            case 2:
            case 5:
                return new Hare(nameForAnimal);
            case 3:
            case 4:
                return new Platypus(nameForAnimal);
            case 6:
            case 9:
                return new Fox(nameForAnimal);
        }
    }
}
