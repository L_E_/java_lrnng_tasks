package com.j2core.elozovan.week04.zoopark;

import com.j2core.elozovan.week04.zoopark.animals.Animal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Code representation of a Cell in the ZooPark.
 *
 * //TODO: Refactor the same way as Animal i.e. introduce BigCell, SmallCell, LuckyCell... .
 */
public class Cell {
    Logger logger = LoggerFactory.getLogger(Cell.class);

    /**
     * Max allowed of food units per animal.
     */
    private static final int MAX_FOOD_AMOUNT_PER_ANIMAL = 544;

    private String cellName;
    private int currentAmountOfFood = 0; //No animals == no food.
    private int countAnimalsLost = 0;

    List<Animal> animalsOfTheCell = new ArrayList<>();

    public Cell() {
        this.cellName = "Cell-" + UUID.randomUUID().toString();
    }

    public Cell(String nameCandidate) {
        this.cellName = nameCandidate;
    }

    public static int getMaxFoodAmountPerAnimal() {
        return MAX_FOOD_AMOUNT_PER_ANIMAL;
    }

    public String getCellName() {
        return cellName;
    }

    public List<Animal> getAnimals() {
        return animalsOfTheCell;
    }

    public void addAnimal(Animal newAnimal) {
        logger.info("~Adding new animal into the Cell '{}': \n {}", cellName, newAnimal);
        animalsOfTheCell.add(newAnimal);
    }

    public void addFood(int amountToAdd) {
        currentAmountOfFood+=amountToAdd;

        // Check whether currentAmountOfFood is below the food threshold.
        if (currentAmountOfFood > animalsOfTheCell.size() * MAX_FOOD_AMOUNT_PER_ANIMAL) {
            currentAmountOfFood = animalsOfTheCell.size() * MAX_FOOD_AMOUNT_PER_ANIMAL;
        }

        logger.info("{{!}}ZooKeeper loves us! The caravan has just brought us "
                + amountToAdd + " units of food and now we have " + currentAmountOfFood + " in total.");
    }

    public int getCurrentAmountOfFood() {
        return currentAmountOfFood;
    }

    public void processMoment() {
        for (Animal animal : animalsOfTheCell) {
            animal.liveForAMoment();

            if (!animal.isSleeping() && animal.isHungry() && animal.isSearchingForFood()) {
                feed(animal);
            }
        }
    }

    public void processCycle() {
        List<Animal> animalsLeavingTheCell = new ArrayList<>();
        for (Animal animal : animalsOfTheCell) {
            animal.liveForACycle();
            if (!animal.isAlive()) {
                animalsLeavingTheCell.add(animal);
            }
        }

        letAnimalsToLeave(animalsLeavingTheCell);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nCell{");
        sb.append("cellName='").append(cellName).append('\'');
        sb.append(", food=").append(currentAmountOfFood);
        sb.append(", animals=").append(animalsOfTheCell);
        sb.append(", animalsLost=").append(countAnimalsLost);
        sb.append('}');
        return sb.toString();
    }

    public void letAllAnimalsToLeave() {
        letAnimalsToLeave(animalsOfTheCell);
    }

    private void feed(Animal animal) {
        logger.info("==========>>>>> Feeding " + animal.getName());
        // The Cell could have not enough food to feed an animal.
        // In that case the rest of the food is to be fed and currentAmountOfFood is to be set to 0.
        int amountOfFoodToFeed = Math.min(animal.getCanEatAtATime(), currentAmountOfFood);
        animal.eat(amountOfFoodToFeed);

        if (0 < currentAmountOfFood) { // Otherwise the Cell could still food from animals. In some cultures that is unacceptable.
            currentAmountOfFood-=amountOfFoodToFeed;
        }
    }

    private void letAnimalsToLeave(List<Animal> animalsLeavingTheCell) {
        if (!animalsLeavingTheCell.isEmpty()) {
            countAnimalsLost+=animalsLeavingTheCell.size();
            animalsOfTheCell.removeAll(animalsLeavingTheCell);
        }
    }
}
