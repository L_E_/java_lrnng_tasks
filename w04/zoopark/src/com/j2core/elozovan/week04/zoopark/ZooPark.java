package com.j2core.elozovan.week04.zoopark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Code representation of the Zoo park itself.
 */
public class ZooPark {
    Logger logger = LoggerFactory.getLogger(ZooPark.class);

    private List<Cell> cells;

    public ZooPark() {
        logger.info("Hiring Dr. Moreau ... .");
        logger.info("Searching for an island for him ... .");
        buildCells();
    }

    public void processCycle() {
        for (Cell cell : cells) {
            // Start a cycle with resource hunting.
            if (cell.getCurrentAmountOfFood() <= Cell.getMaxFoodAmountPerAnimal()) {
                playFoodLottery(cell);
            }

            cell.processCycle();

            // REQ.85 If you have more than XX amount of food in the end of the day - all animals of the Cell
            //  will leave Zoo.
            int leaveThreshold = cell.getAnimals().size() * 19 * Cell.getMaxFoodAmountPerAnimal() / 20;
            if (cell.getAnimals().size() > 0 && cell.getCurrentAmountOfFood() > leaveThreshold) {
                logger.warn("\n{{!}}\n It is too good here to be true. All animals are leaving now in despair.");
                cell.letAllAnimalsToLeave();
            }

            logger.info("\nCell's state by the end of the cycle: " + cell);
        }
    }

    public void processMoment() {
        for (Cell cell : cells) {
            logger.info(" Inspecting Cell " + cell.getCellName());
            cell.processMoment();
            logger.info("{{!}} Cell state by the end of the moment:" + cell);
        }
    }

    @Override
    public String toString() {
        return "ZooPark{" + "cells=" + cells + '}';
    }

    /**
     * Initialization - creating cells and populating them with animals.
     */
    private void buildCells() {
        //A Zoo shall have at least one Cell(N_of_Cell -> from 1 to N)
        cells = new ArrayList<>();
        cells.add(addAnimalsAndFoodForThem(new Cell("C00")));
        cells.add(addAnimalsAndFoodForThem(new Cell()));
        cells.add(addAnimalsAndFoodForThem(new Cell("Kl_O-O")));
    }

    /**
     * Initializing a cell:
     * - populate with animals;
     * - provide some food.
     *
     */
    private Cell addAnimalsAndFoodForThem(Cell cell) {
        Random rnd = new Random();
        int numberOfAnimalsToAdd = 1 + rnd.nextInt(8 + cells.size()); // Need at least one animal per Cell.

        for (int i =0; i < numberOfAnimalsToAdd; i++) {
            String composedAnimalName = getANameForAnimal(cell.getCellName(), i);
            cell.addAnimal(AnimalFactory.create(composedAnimalName));
        }

        cell.addFood(numberOfAnimalsToAdd
                * (Cell.getMaxFoodAmountPerAnimal() / 2 + rnd.nextInt(Cell.getMaxFoodAmountPerAnimal() / 2)));

        return cell;
    }

    private String getANameForAnimal(String cellName, int mark) {
        String name;
        if (new Random().nextDouble() >= 0.3) {
            String[] candidates = getNameCandidates();
            int index = new Random().nextInt(candidates.length);
            name = candidates[index];
        } else {
            name = cellName.replace("Cell-", "").substring(0, Math.min(4, cellName.length()));
            name = "A-" + name + "-" + mark;
        }

        return name;
    }

    private String[] getNameCandidates() {
        return new String[]{"Umka", "Beluga", "Revjaka",
                            "Xrjak", "Brjak", "VseYak",
                            "Xudozhnik", "Zalipuha", "Dazdraperma",
                            "Krasnuha", "4u4a", "Vyhuholina",
                            "ShmYak", "Ziza", "Abvgdejka",
                            "Vypolzen'", "Planerjuga", "Podkustovnik",
                            "Zanuda"};
    }

    /**
     * " *   [x]REQ.75 Then you have to create a process which will drive all this emulation,
     *   for example will emulate 1 week of the Zoo with 1 hour interval.
     *
     *   P.S. It’s a question how frequently and how much food need to be added."
     *
     *   The method uses Random.netGaussian() to decide whether some food could be added into the cell.
     *   To add, not to add, how mach to add is defined by dynamically calculated threshold.
     *
     *
     * @param lotteryParticipant - the cell participating in the food lottery.
     */
    private void playFoodLottery(Cell lotteryParticipant) {
        Random rnd = new Random();
        double value = rnd.nextGaussian();
        double currentAmount = lotteryParticipant.getCurrentAmountOfFood();
        double maxPossibleAmount = lotteryParticipant.getAnimals().size() * Cell.getMaxFoodAmountPerAnimal();
        double threshold = rnd.nextDouble()
                            - (maxPossibleAmount - currentAmount) / (maxPossibleAmount + currentAmount);

        if (value >= threshold) {
            // Amount of food to add depends on count of animals in the cell.
            lotteryParticipant.addFood(lotteryParticipant.getAnimals().size()
                    * (Cell.getMaxFoodAmountPerAnimal() / 5 + rnd.nextInt(Cell.getMaxFoodAmountPerAnimal() / 5)));
        } else if (value <= -threshold) {
            lotteryParticipant.addFood(lotteryParticipant.getAnimals().size()
                    * (Cell.getMaxFoodAmountPerAnimal() / 4 + rnd.nextInt(Cell.getMaxFoodAmountPerAnimal() / 3)));
        }
        //By default and most likely - give no food.
    }
}
