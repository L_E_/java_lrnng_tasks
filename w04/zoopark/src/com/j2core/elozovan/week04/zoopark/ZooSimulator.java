package com.j2core.elozovan.week04.zoopark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The main simulator class.
 *
 * Covers REQ.75 Then you have to create a process which will drive all this emulation,
 *   for example will emulate 1 week of the Zoo with 1 hour interval.
 *
 *   [x]REQ.00 You need a Zoo with few Cells.
 *   [x]REQ.05 Few portions of food can be added to the Cell and
 *   [x]REQ.10 Cell, obviously, need some ability to track how many food remaining.
 *   [x]REQ.15 Also you need Animals.
 *   [x]REQ.20 Every animal can eat,
 *   [x]REQ.25 sleep and
 *   []REQ.30 move to find food.
 *   []REQ.35 When animal finds a food (some probability percent) it can eat it if hungry.
 *   [x]REQ.40 So, every animal need to have at least 2 parameters to track sleep and hungry.
 *   [x]REQ.45 Every hour animal need to decide what to do depending on internal state - search for food
 *  or sleep or do nothing.
 *   [x]REQ.50 Probably every hour, when animal active should increase some
 *  internal parameter which will lead (after some threshold reached) to sleep.
 *   [x]REQ.55  It’s obviously that do nothing and move should increase this parameter on different values.
 *
 *   You can add more parameters to emulate behaviour, but described is enough.
 *   [x]REQ.60 Every animal will have own behaviour, which actions to do depending on internal state.
 *   [x]REQ.65 Now you have to understand that different animals will have different internal parameters,
 *   thresholds etc.
 *   [x]REQ.70 Also different animals will eat different amount of food.
 *   [x]REQ.75 Then you have to create a process which will drive all this emulation,
 *   for example will emulate 1 week of the Zoo with 1 hour interval.
 *
 *   P.S. It’s a question how frequently and how much food need to be added.
 *   [x]REQ.80 If animal will be hungry (below some threshold) more than 24h it will leave Zoo.
 *   [x]REQ.85 If you have more than XX amount of food in the end of the day - all animals of the Cell
 *   will leave Zoo.
 */
public class ZooSimulator {
    static Logger logger = LoggerFactory.getLogger(ZooSimulator.class);

    public final static double VERSION = 2.1;
    private final static int MOMENT = 1;
    private final static int CYCLE = 24 * MOMENT;
    private final static int LIFESPAN = 7 * CYCLE; // For how long we are going to torture those animals.

    /**
     * The simulation driver.
     */
    public static void main(String[] args) {
        printInitialMessage();
        ZooPark zooPark = new ZooPark();

        for (int cycle = 0; cycle < LIFESPAN; cycle+= CYCLE) { // Longer cycles.
            printNewCycleMessage(cycle);
            for (int moment = 0; moment < CYCLE; moment+= MOMENT) { // Moments of life.
                printNewMomentMessage(cycle, moment);
                zooPark.processMoment();
            }

            zooPark.processCycle();
        }

        printFinalMessage(zooPark);
    }

    private static void printInitialMessage() {
        logger.info("\n{{{!}}}\nThe astrologists say it might be a good week for animal population. " +
                "Or it might be not.\n Everything in hands of the GBR (a.k.a ВБР).");
    }

    private static void printNewMomentMessage(int cycle, int moment) {
        logger.info("<--- Cycle {}, moment {}. --->\n", cycle / CYCLE, moment);
    }

    private static void printNewCycleMessage(int cycle) {
        logger.info("\n==========================================");
        logger.info("<=== New cycle is about to begin. N{}. ===>\n", cycle / CYCLE);
    }

    private static void printFinalMessage(ZooPark zooPark) {
        logger.info("\n{{{!}}}\nThat cruel simulation has came to a point. And the point is: \n" + zooPark);
    }
}