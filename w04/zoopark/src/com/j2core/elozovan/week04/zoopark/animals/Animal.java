package com.j2core.elozovan.week04.zoopark.animals;

import com.j2core.elozovan.week04.zoopark.enums.AnimalWhatToDo;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Code representation of an Animal in the ZooPark.
 *
 */
public abstract class Animal {
    Logger logger = LoggerFactory.getLogger(Animal.class);

    public static final int MAX_COUNT_OF_CYCLES_BEFORE_LEAVE = 24;

    protected String name;

    protected boolean isSleeping;
    protected boolean isHungry;
    protected boolean isSearchingForFood;
    protected boolean isAlive;

    protected int staminaLevel; //!fatigue. Defines whether this animal active or sleepy.
    protected int sleepStaminaThreshold; //Id staminaLevel <= the threshold then this animal falls asleep.
    protected int individualStaminaDeltaUpnSearch; // Each animal has its own tiring velocity.
    protected int individualStaminaDeltaUpnDoNothing; // Each animal has its own tiring velocity.
    protected int individualStaminaDeltaUpnSleep; // Each animal has its own restoring velocity.
    protected int individualStaminaDeltaForLifeCosts; // Each animal has its own stamina cost for simply living.

    protected int hungryLevel;
    protected int canEatAtATime;// Max amount of hungryLevel points this animal could "eat" at a time.
    protected int starvationThreshold;// if hungryLevel > starvationThreshold the this animal is on the way to leave.
    protected int isHungryFor;// Count of cycles this animal is being hungry.

    public Animal(String nameCandidate) {
        this.name = nameCandidate;
        init();
    }

    public abstract String getSpeciesName ();

    public String getName() {
        return name;
    }

    public String getTitle() {
        return getSpeciesName() + " " + getName();
    }

    public boolean isSleeping() {
        return isSleeping;
    }

    public boolean isHungry() {
        return isHungry;
    }

    public boolean isSearchingForFood() {
        return isSearchingForFood;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public int getCanEatAtATime() {
        return canEatAtATime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nAnimal{");
        sb.append("Species='").append(getSpeciesName()).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", isSleeping=").append(isSleeping);
        sb.append(", isHungry=").append(isHungry);
        sb.append(", isHungryFor=").append(isHungryFor);
        sb.append(", isSearchingForFood=").append(isSearchingForFood);
        sb.append(", staminaLevel=").append(staminaLevel);
        sb.append(", hungryLevel=").append(hungryLevel);
        sb.append(", isAlive=").append(isAlive);
        sb.append(", canEatAtATime=").append(canEatAtATime);
        sb.append(", starvationThreshold=").append(starvationThreshold);
        sb.append(", sleepStaminaThreshold=").append(sleepStaminaThreshold);
        sb.append(", individualStaminaDeltaUpnDoNothing=").append(individualStaminaDeltaUpnDoNothing);
        sb.append(", individualStaminaDeltaUpnSleep=").append(individualStaminaDeltaUpnSleep);
        sb.append('}');
        return sb.toString();
    }

    /**
     * "Eating". Decreases hungryLevel, increases staminaLevel.
     *
     * @param foodItems - how many food items somebody gave to the animal.
     */
    public void eat(int foodItems) {
        hungryLevel-=foodItems;
        if (0 > hungryLevel) {
            hungryLevel = 0; // Do not want for an animal to be over-fed.
        }

        //Food brings energy.
        staminaLevel+= individualStaminaDeltaUpnDoNothing;
        checkHungryLevel();
        logger.info(" <<<<<<< {}, ate {} units of food. Is it hungry now? {}",
                getTitle(), foodItems, (isHungry ? "Yes." : "No."));
    }

    public void liveForAMoment() {
        checkStaminaLevel();

        if (!isSleeping()) {
            AnimalWhatToDo whatToDoAtThisVeryMoment = decideWhatToDo();
            logger.info("{} decides to {}", getTitle(), whatToDoAtThisVeryMoment);
            switch (whatToDoAtThisVeryMoment) {
                case SEARCH_FOR_FOOD:
                    doSearch();
                    break;
                case SLEEP:
                    doSleep();
                    break;
                case DO_NOTHING:
                default:
                    doNothing();
                    break;
            }
        } else {
            doSleep();
        }

        doLifeCosts();
        checkHungryLevel();
    }

    public void liveForACycle() {
        //REQ.85 If you have more than XX amount of food in the end of the day - all animals of the Cell
        if (isHungryFor >= MAX_COUNT_OF_CYCLES_BEFORE_LEAVE) {
            logger.warn("}}}}}}}}}}}}} Kirdyk {}", name);
            isAlive = false;
        }
    }

    /**
     * Gets the minimum minimorum of the stamina level when any animal of the kind falls asleep.
     */
    protected abstract int getSleepThreshold();

    /**
     * Gets the maximum maximorum stamina level possible for the species in the ZooWorld.
     */
    protected abstract int getMaxStaminaLevel();

    /**
     * An animals is resting with some velocity when it is sleeping.
     * The method returns amount of stamina restoration per moment.
     */
    protected abstract int getStaminaDeltaPerMomentWhenSleeping();

    /**
     * An animals is tiring with some velocity when it is awake.
     * The method returns amount of stamina degradation per moment.
     */
    protected abstract int getStaminaDeltaPerMomentWhenActive();

    protected abstract int getStaminaDeltaPerMomentWhenLive();

    /**
     * Maximum possible starvation threshold for the species. Each animal has its own threshold <= MAX one.
     */
    protected abstract int getMaxStarvationThreshold();

    protected abstract int getHungryLvlDeltaUponSearching();

    protected abstract int getHungryLvlDeltaUponDoingNothing();

    protected abstract int getHungryLvlDeltaUponSleeping();

    protected abstract int getHungryLvlDeltaForLifeCosts();

    protected void init() {
        Random rnd = new Random();

        this.isSleeping = false; // It is quite active upon its "birth".
        this.isHungry = false; // It is not hungry upon its "birth".
        this.isSearchingForFood = false; // It is full initially.
        this.isAlive = true;

        // Set initial individual values for the counters.
        this.sleepStaminaThreshold = getSleepThreshold() + rnd.nextInt(getSleepThreshold());
        this.staminaLevel = this.sleepStaminaThreshold + rnd.nextInt(getMaxStaminaLevel() - this.sleepStaminaThreshold);
        this.individualStaminaDeltaUpnSearch = getStaminaDeltaPerMomentWhenActive()
                + rnd.nextInt(getSleepThreshold());
        this.individualStaminaDeltaUpnDoNothing = getStaminaDeltaPerMomentWhenActive()
                + rnd.nextInt(getSleepThreshold());
        this.individualStaminaDeltaUpnSleep = getStaminaDeltaPerMomentWhenSleeping()
                + rnd.nextInt(getSleepThreshold());
        this.individualStaminaDeltaForLifeCosts = getStaminaDeltaPerMomentWhenLive()
                + rnd.nextInt(getSleepThreshold());

        this.hungryLevel = 0; // Not hungry initially.
        this.starvationThreshold = getMaxStarvationThreshold() / 2 + rnd.nextInt(getMaxStarvationThreshold() / 2);
        this.canEatAtATime = getHungryLvlDeltaUponSearching() + this.starvationThreshold / 10 +  rnd.nextInt(this.starvationThreshold / 5);
        this.isHungryFor = 0;
    }

    /**
     * [x]REQ.45 Every hour animal need to decide what to do depending on internal state - search for food
     *  or sleep or do nothing.
     *
     *  Based on Gaussian distribution.
     *    /\
     *   /  \
     * /| | |\
     *  a b c
     *  where a - a stamina threshold. If random value > abs(a) then the animal decides to sleep;
     *        c - a hungry threshold. If random value <= abs(c) then the animal decides to search for food;
     *        b - mean. No special action but if random value does not meet conditions a or c then the animal decides to do nothing;
     * Note: a and c are calculated basing on current stamina and hungry level in such a way that
     *  1. Starvation increases probability of "go find something to eat".
     *  2. When tired - the animal tends to fall asleep.
     *
     * [x]REQ.60 Every animal will have own behaviour, which actions to do depending on internal state.
     *
     * @return - what-to-do.
     */
    protected AnimalWhatToDo decideWhatToDo() {
        Random rnd = new Random();
        double value = rnd.nextGaussian();

        //Starvation increases probability of "go find something to eat".
        double hungryThreshold = (double) (starvationThreshold - hungryLevel) / (double) (starvationThreshold + hungryLevel);

        // When tired - it tends to fall asleep.
        double staminaThreshold = (double) (sleepStaminaThreshold - staminaLevel) / (double) (sleepStaminaThreshold + staminaLevel);

        if (Math.abs(value) >= hungryThreshold) {
            return AnimalWhatToDo.SEARCH_FOR_FOOD;
        } else if (Math.abs(value) < staminaThreshold) {
            return AnimalWhatToDo.SLEEP;
        }

        // Most likely to select this one (as it corresponds to Gaussian mean value).
        return AnimalWhatToDo.DO_NOTHING;
    }

    /**
     * Models base "life costs" - an animal spends some energy etc
     * just to keep living independently of what it is doing.
     */
    protected void doLifeCosts() {
        staminaLevel -= individualStaminaDeltaForLifeCosts;
        hungryLevel += getHungryLvlDeltaForLifeCosts();
    }

    protected void doNothing() {
        isSleeping = false;
        staminaLevel -= individualStaminaDeltaUpnDoNothing;
        hungryLevel += getHungryLvlDeltaUponDoingNothing();
        isSearchingForFood = false;
    }

    protected void doSearch() {
        isSleeping = false;
        staminaLevel -= individualStaminaDeltaUpnSearch;
        hungryLevel += getHungryLvlDeltaUponSearching();
        isSearchingForFood = true;
    }

    protected void doSleep() {
        isSleeping = true;
        staminaLevel += individualStaminaDeltaUpnSleep;
        hungryLevel += getHungryLvlDeltaUponSleeping();
    }

    protected void checkHungryLevel() {
        if (hungryLevel >= starvationThreshold) {
            logger.warn(" <<<<<<< " + getTitle() + " is hungry.");
            isHungry = true;
            isHungryFor++; //It is gonna leave soon... .
        } else {
            isHungry = false;
            isHungryFor = 0;
        }
    }

    protected void checkStaminaLevel() {
        // Is it tired?
        if (!isSleeping() && staminaLevel <= sleepStaminaThreshold) {
            isSleeping = true;
            logger.info(getTitle() + " falls asleep. Poor " + getSpeciesName() + ".");
        } else if (isSleeping()) {
            isSleeping = false;
            logger.info(getTitle() + " has just woke up. Is it a time to feed it up?");
        }
    }
}
