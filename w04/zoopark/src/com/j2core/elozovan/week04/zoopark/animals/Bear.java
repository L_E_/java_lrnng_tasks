package com.j2core.elozovan.week04.zoopark.animals;

/**
 * Represents Bears.
 */
public class Bear extends Animal {

    public Bear(String nameForAnimal) { super(nameForAnimal); }

    @Override
    public String getSpeciesName() { return "Bear"; }

    @Override
    protected int getSleepThreshold() { return 3; }

    @Override
    protected int getMaxStaminaLevel() { return 8 * getSleepThreshold() + 25; }

    @Override
    protected int getStaminaDeltaPerMomentWhenSleeping() { return getSleepThreshold() / 2 + 2; }

    @Override
    protected int getStaminaDeltaPerMomentWhenActive() { return  getSleepThreshold() / 6 + 1; }

    @Override
    protected int getStaminaDeltaPerMomentWhenLive() { return getSleepThreshold() / 9 + 1; }

    @Override
    protected int getMaxStarvationThreshold() { return 45; }

    @Override
    protected int getHungryLvlDeltaUponSearching() { return  3 + getMaxStarvationThreshold()/ 20; }

    @Override
    protected int getHungryLvlDeltaUponDoingNothing() { return  2 + getMaxStarvationThreshold() / 20; }

    @Override
    protected int getHungryLvlDeltaUponSleeping() { return 1 + getMaxStarvationThreshold() / 20; }

    @Override
    protected int getHungryLvlDeltaForLifeCosts() { return getMaxStarvationThreshold() / 27; }
}