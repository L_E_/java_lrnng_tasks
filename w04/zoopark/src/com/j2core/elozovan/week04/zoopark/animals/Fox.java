package com.j2core.elozovan.week04.zoopark.animals;

import com.j2core.elozovan.week04.zoopark.enums.AnimalWhatToDo;

import java.util.Random;

/**
 * Represents Foxes.
 */
public class Fox extends Animal {

    public Fox(String nameForAnimal) { super(nameForAnimal); }

    @Override
    public String getSpeciesName() {
        return "Fox";
    }

    @Override
    protected int getSleepThreshold() {
        return 21;
    }

    @Override
    protected int getMaxStaminaLevel() {
        return 63;
    }

    @Override
    protected int getStaminaDeltaPerMomentWhenSleeping() {
        return 3;
    }

    @Override
    protected int getStaminaDeltaPerMomentWhenActive() {
        return 5;
    }

    @Override
    protected int getStaminaDeltaPerMomentWhenLive() {
        return 1;
    }

    @Override
    protected int getMaxStarvationThreshold() {
        return 36;
    }

    @Override
    protected int getHungryLvlDeltaUponSearching() {
        return 2;
    }

    @Override
    protected int getHungryLvlDeltaUponDoingNothing() {
        return 1;
    }

    @Override
    protected int getHungryLvlDeltaUponSleeping() {
        return 1;
    }

    @Override
    protected int getHungryLvlDeltaForLifeCosts() {
        return 1;
    }

    protected AnimalWhatToDo decideWhatToDo() {
        Random rnd = new Random();
        int value = rnd.nextInt(getMaxStarvationThreshold());

        if (value > hungryLevel) {
            return AnimalWhatToDo.SEARCH_FOR_FOOD;
        } else if (value < hungryLevel / 2) {
            return AnimalWhatToDo.SLEEP;
        }

        return AnimalWhatToDo.DO_NOTHING;
    }
}