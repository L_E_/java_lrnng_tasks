package com.j2core.elozovan.week04.zoopark.animals;

/**
 * Represents Hares.
 */
public class Hare extends Animal {

    public Hare(String nameForAnimal) { super(nameForAnimal); }

    @Override
    public String getSpeciesName() { return "Hare"; }

    @Override
    protected int getSleepThreshold() {
        return 7;
    }

    @Override
    protected int getMaxStaminaLevel() { return 2 * getSleepThreshold() + 10; }

    @Override
    protected int getStaminaDeltaPerMomentWhenSleeping() {
        return getSleepThreshold() / 2 + 1;
    }

    @Override
    protected int getStaminaDeltaPerMomentWhenActive() {
        return  getSleepThreshold() / 2 + 2;
    }

    @Override
    protected int getStaminaDeltaPerMomentWhenLive() {
        return getSleepThreshold() / 4 + 1;
    }

    @Override
    protected int getMaxStarvationThreshold() {
        return 17;
    }

    @Override
    protected int getHungryLvlDeltaUponSearching() {
        return  3 + getMaxStarvationThreshold()/ 13;
    }

    @Override
    protected int getHungryLvlDeltaUponDoingNothing() {
        return  2 + getMaxStarvationThreshold() / 15;
    }

    @Override
    protected int getHungryLvlDeltaUponSleeping() {
        return 1 + getMaxStarvationThreshold() / 16;
    }

    @Override
    protected int getHungryLvlDeltaForLifeCosts() {
        return getMaxStarvationThreshold() / 30;
    }
}