package com.j2core.elozovan.week04.zoopark.animals;

/**
 * Represents Lions.
 */
public class Lion extends Animal {

    public Lion(String nameForAnimal) { super(nameForAnimal); }

    @Override
    public String getSpeciesName() { return "Lion"; }

    @Override
    protected int getSleepThreshold() { return 5; }

    @Override
    protected int getMaxStaminaLevel() { return 7 * getSleepThreshold() + 20; }

    @Override
    protected int getStaminaDeltaPerMomentWhenSleeping() { return getSleepThreshold() / 2 + 1; }

    @Override
    protected int getStaminaDeltaPerMomentWhenActive() { return  getSleepThreshold() / 5 + 1; }

    @Override
    protected int getStaminaDeltaPerMomentWhenLive() { return getSleepThreshold() / 8 + 1; }

    @Override
    protected int getMaxStarvationThreshold() { return 29; }

    @Override
    protected int getHungryLvlDeltaUponSearching() { return  3 + getMaxStarvationThreshold()/ 15; }

    @Override
    protected int getHungryLvlDeltaUponDoingNothing() { return  2 + getMaxStarvationThreshold() / 15; }

    @Override
    protected int getHungryLvlDeltaUponSleeping() { return 1 + getMaxStarvationThreshold() / 20; }

    @Override
    protected int getHungryLvlDeltaForLifeCosts() { return getMaxStarvationThreshold() / 21; }
}