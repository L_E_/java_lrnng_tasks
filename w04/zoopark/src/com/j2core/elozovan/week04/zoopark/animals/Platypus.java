package com.j2core.elozovan.week04.zoopark.animals;

import com.j2core.elozovan.week04.zoopark.enums.AnimalWhatToDo;

import java.util.Random;

/**
 * Represents Platypuses.
 */
public class Platypus extends Animal {

    public Platypus(String nameForAnimal) { super(nameForAnimal); }

    @Override
    public String getSpeciesName() { return "Duck-bill"; }

    @Override
    protected int getSleepThreshold() { return 13; }

    @Override
    protected int getMaxStaminaLevel() { return 2 * getSleepThreshold() + 30; }

    @Override
    protected int getStaminaDeltaPerMomentWhenSleeping() { return getSleepThreshold() / 3 + 1; }

    @Override
    protected int getStaminaDeltaPerMomentWhenActive() { return getSleepThreshold() / 4 + 2; }

    @Override
    protected int getStaminaDeltaPerMomentWhenLive() { return getSleepThreshold() / 7 + 1; }

    @Override
    protected int getMaxStarvationThreshold() { return 55; }

    @Override
    protected int getHungryLvlDeltaUponSearching() { return 3 + getMaxStarvationThreshold()/ 13; }

    @Override
    protected int getHungryLvlDeltaUponDoingNothing() { return 2 + getMaxStarvationThreshold() / 15; }

    @Override
    protected int getHungryLvlDeltaUponSleeping() { return 1 + getMaxStarvationThreshold() / 16; }

    @Override
    protected int getHungryLvlDeltaForLifeCosts() { return getMaxStarvationThreshold() / 30; }

    /**
     * "Eating". Decreases hungryLevel.
     */
    @Override
    public void eat(int foodItems) {
        hungryLevel-= foodItems;
        if (-2 > hungryLevel) {
            hungryLevel = 0;
        }

        checkHungryLevel();
        System.out.println(" <<<<<<< " + getTitle() + " ate " + foodItems + " tons of food. ");
    }

    /**
     * [x]REQ.45 Every hour animal need to decide what to do depending on internal state - search for food
     *  or sleep or do nothing.
     *
     *  Based on pseudorandom, uniform distribution.
     *
     * @return
     */
    protected AnimalWhatToDo decideWhatToDo() {
        Random rnd = new Random();
        double value = rnd.nextDouble();

        //Starvation should increase probability of "go find something to eat".
        double hungryThreshold = (double) (starvationThreshold - hungryLevel) / (double) (starvationThreshold + hungryLevel);

        // When tired - it tends to fall asleep.
        double staminaThreshold = (double) (sleepStaminaThreshold - staminaLevel) / (double) (sleepStaminaThreshold + staminaLevel);

        if (value > hungryThreshold) {
            return AnimalWhatToDo.SEARCH_FOR_FOOD;
        } else if (value < staminaThreshold) {
            return AnimalWhatToDo.SLEEP;
        }

        return AnimalWhatToDo.DO_NOTHING;
    }

    @Override
    protected void doNothing() {
        isSleeping = false;
        staminaLevel -= individualStaminaDeltaUpnDoNothing;
        hungryLevel -= getHungryLvlDeltaUponDoingNothing();
        isSearchingForFood = false;
    }

    @Override
    protected void checkStaminaLevel() {
        // Is it tired?
        if (staminaLevel < sleepStaminaThreshold) {
            isSleeping = true;
            System.out.println(getTitle() + " is sleeping.");
        } else {
            isSleeping = false;
            System.out.println(getTitle() + " is active.");
        }
    }
}
