package com.j2core.elozovan.week04.zoopark.enums;

/**
 * Defines possible actions for an animal.
 */
public enum AnimalWhatToDo {
    SEARCH_FOR_FOOD,
    SLEEP,
    DO_NOTHING
}
