package com.j2core.elozovan.week04.zoopark.tests;

import com.j2core.elozovan.week04.zoopark.AnimalFactory;
import com.j2core.elozovan.week04.zoopark.animals.Animal;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AnimalFactoryTest {

    @Test
    public void testCreateWithDefaultName() throws Exception {
        Animal animal = AnimalFactory.create();
        assertTrue(null != animal);
        assertFalse(animal.getName().isEmpty());
        assertFalse(animal.getSpeciesName().isEmpty());
        assertTrue(animal.isAlive());
        assertFalse(animal.isHungry());
        assertFalse(animal.isSleeping());
    }

    @Test
    public void testCreateWitCustomName() throws Exception {
        String expectedName = "Barsik";
        Animal animal = AnimalFactory.create(expectedName);
        assertTrue(null != animal);
        assertEquals(animal.getName(), expectedName);
    }
}