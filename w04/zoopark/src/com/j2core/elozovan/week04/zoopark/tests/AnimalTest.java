package com.j2core.elozovan.week04.zoopark.tests;

import com.j2core.elozovan.week04.zoopark.AnimalFactory;
import com.j2core.elozovan.week04.zoopark.animals.Animal;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AnimalTest {

    @Test
    public void testGetTitle() throws Exception {
        Animal animal = AnimalFactory.create();
        assertFalse(animal.getTitle().isEmpty());
    }

    @Test
    public void testEat() throws Exception {
        Animal animal = AnimalFactory.create();
        animal.eat(100500);
        assertFalse(animal.isHungry());
    }
}