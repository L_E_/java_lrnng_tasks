package com.j2core.elozovan.week04.zoopark.tests;

import com.j2core.elozovan.week04.zoopark.AnimalFactory;
import com.j2core.elozovan.week04.zoopark.Cell;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CellTest {

    @Test
    public void testGetMaxFoodAmountPerAnimal() throws Exception {
        Assert.assertTrue(Cell.getMaxFoodAmountPerAnimal() >= 0);
    }

    @Test
    public void testGetCellName() throws Exception {
        Cell cell = new Cell();
        Assert.assertFalse(cell.getCellName().isEmpty());

        String expectedName = "KL";
        cell = new Cell(expectedName);
        Assert.assertEquals(cell.getCellName(), expectedName);
    }

    @Test
    public void testGetAnimalsWithoutAddingThem() throws Exception {
        Cell cell = new Cell();
        Assert.assertTrue(cell.getAnimals().isEmpty());
    }

    @Test
    public void testAddAnimal() throws Exception {
        Cell cell = new Cell();
        cell.addAnimal(AnimalFactory.create());
        Assert.assertFalse(cell.getAnimals().isEmpty());
        Assert.assertEquals(cell.getAnimals().size(), 1);
    }

    @Test
    public void testAddFood() throws Exception {
        Cell cell = new Cell();
        int before = cell.getCurrentAmountOfFood();
        cell.addFood(1);
        int after = cell.getCurrentAmountOfFood();
        Assert.assertEquals(after, before); //No animals => cannot add food.

        cell.addAnimal(AnimalFactory.create());
        before = cell.getCurrentAmountOfFood();
        cell.addFood(1);
        after = cell.getCurrentAmountOfFood();
        Assert.assertEquals(after, before + 1);
    }

    @Test
    public void testLetAllAnimalsToLeave() throws Exception {
        Cell cell = new Cell();
        cell.addAnimal(AnimalFactory.create());
        cell.addAnimal(AnimalFactory.create());
        cell.letAllAnimalsToLeave();
        Assert.assertTrue(cell.getAnimals().isEmpty());
    }
}