package com.j2core.elozovan.week04.zoopark3;

import com.j2core.elozovan.week04.zoopark3.animals.Animal;
import com.j2core.elozovan.week04.zoopark3.cages.Cage;
import com.j2core.elozovan.week04.zoopark3.enums.AnimalState;
import com.j2core.elozovan.week04.zoopark3.util.NameProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * This is a ZooKeeper. He keeps the ZooPark under control.
 * ZooKeeper is responsible for inspection of cages, feeding and removing animals.
 * Also, ZooKeeper might add some food into cages.
 */
public class ZooKeeper {
    Logger logger = LoggerFactory.getLogger(ZooKeeper.class);
    private final static int FOOD_AMOUNT_MIN_THRESHOLD = 288;
    private String name;

    /**
     * A food-per-cage threshold. Individual for each zoo-keeper.
     * If cage.foodAmount < the Threshold then addFoodMaybe.
     */
    private int foodAmountThreshold;

    public ZooKeeper() {
        this.name = NameProvider.getANameForZooKeeper();
        this.foodAmountThreshold = FOOD_AMOUNT_MIN_THRESHOLD + new Random().nextInt(Calendar.getInstance().get(Calendar.MILLISECOND) );
    }

    public String getName() {
        return name;
    }

    /**
     * Inspects cages.
     * Feeds animals if needed.
     * Might add some food into a cage.
     * Let's animals to leave the Zoo.
     */
    public void inspect(List<Cage> cages) {
        for (Cage cage : cages) {
            // Start a cycle with resource hunting.
            if (cage.getCurrentFoodAmount() <= foodAmountThreshold) {
                playFoodLottery(cage);
            }

            for (Animal animal : cage.getAnimals()) {
                inspectAnimal(cage, animal);
            }

            // REQ.85 If you have more than XX amount of food in the end of the day - all animals of the Cell
            //  will leave Zoo.
            int leaveThreshold = cage.getAnimals().size() * 2 * foodAmountThreshold;
            if (cage.getAnimals().size() > 0 && cage.getCurrentFoodAmount() > leaveThreshold) {
                logger.warn("\n<-!->\n It is too good here to be true. All animals of {} are leaving now in despair.", cage.getName());
                cage.letAllAnimalsToLeave();
            }

            logger.info("\nInspection result: Cage's state by the end of the hour: " + cage);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZooKeeper{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    /**
     * " *   [x]REQ.75 Then you have to create a process which will drive all this emulation,
     *   for example will emulate 1 week of the Zoo with 1 hour interval.
     *
     *   P.S. It’s a question how frequently and how much food need to be added."
     *
     *   The method uses Random.netGaussian() to decide whether some food could be added into the cell.
     *   To add, not to add, how mach to add is defined by dynamically calculated threshold.
     *
     *
     * @param lotteryParticipant - the cell participating in the food lottery.
     */
    private void playFoodLottery(Cage lotteryParticipant) {
        Random rnd = new Random();
        double value = rnd.nextGaussian();
        double currentAmount = lotteryParticipant.getCurrentFoodAmount();
        double maxPossibleAmount = lotteryParticipant.getAnimals().size() * foodAmountThreshold / 2;
        double threshold = rnd.nextDouble()
                - (maxPossibleAmount - currentAmount) / (maxPossibleAmount + currentAmount);

        if (value >= threshold) {
            // Amount of food to add depends on count of animals in the cell.
            lotteryParticipant.addFood(foodAmountThreshold);
        } else if (value <= -threshold) {
            lotteryParticipant.addFood(foodAmountThreshold / 2);
        }
        //By default and most likely - give no food.
    }

    private void inspectAnimal(Cage cage, Animal animal) {
        animal.live();

        if (!animal.isSleeping() && animal.isHungry() && animal.getState().equals(AnimalState.SEARCHES_FOR_FOOD)) {
            feed(cage, animal);
        }

        if (!animal.isAlive()) {
            cage.getAnimals().remove(animal); //Should be ok as long as animals are CopyOnTheWriteList.
        }
    }

    private void feed(Cage cage, Animal animal) {
        logger.info("==========>>>>> Feeding " + animal.getTitle());
        animal.eat(cage.deductFood(animal.getMaxAmountOfFoodItCanEat()));
    }
}
