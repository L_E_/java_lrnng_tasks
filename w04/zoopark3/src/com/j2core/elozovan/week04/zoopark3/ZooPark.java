package com.j2core.elozovan.week04.zoopark3;

import com.j2core.elozovan.week04.zoopark3.cages.Cage;
import com.j2core.elozovan.week04.zoopark3.util.AnimalFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Represents ZooPark itself. I.e. contains cages which in turn contain animals.
 * Also, employs ZooKeeper which is responsible for inspection of cages, feeding and removing animals.
 * Also, ZooKeeper might add some food into cages.
 */
public class ZooPark {
    Logger logger = LoggerFactory.getLogger(ZooPark.class);

    private ZooKeeper zooKeeper;
    private List<Cage> cages;

    public ZooKeeper hireZooKeeper() {
        zooKeeper = new ZooKeeper();
        logger.info("Hired new zoo-keeper - {}.", zooKeeper.getName());

        return zooKeeper;
    }

    public List<Cage> buildCages() {
        logger.info("Building new brilliant comfortable and modern cages.");
        cages = new ArrayList<>();
        cages.add(new Cage("K1"));
        cages.add(new Cage("K2"));
        cages.add(new Cage("K3"));

        return cages;
    }

    public void populateWithAnimals() {
        for (Cage cage : cages) {
            Random rnd = new Random();
            int numberOfAnimalsToAdd = 1 + rnd.nextInt(8 + cages.size()); // Need at least one animal per Cell.

            for (int i =0; i < numberOfAnimalsToAdd; i++) {
                cage.addAnimal(AnimalFactory.create());
            }
        }
    }

    public void function() {
        logger.info("The time has come to glance over the territory, cages and animals.");
        zooKeeper.inspect(cages);
    }

    public List<Cage> getCages() {
        return cages;
    }

    public ZooKeeper getZooKeeper() {
        return zooKeeper;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZooPark{");
        sb.append("zooKeeper=").append(zooKeeper);
        sb.append(", cages=").append(cages);
        sb.append('}');
        return sb.toString();
    }
}