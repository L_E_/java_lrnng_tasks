package com.j2core.elozovan.week04.zoopark3.animals;

import com.j2core.elozovan.week04.zoopark3.enums.AnimalKind;
import com.j2core.elozovan.week04.zoopark3.enums.AnimalState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Abstract animal having base functions implemented.
 */
public abstract class Animal {
    Logger logger = LoggerFactory.getLogger(Animal.class);

    protected static final int MAX_COUNT_OF_CYCLES_BEFORE_LEAVE = 24;

    protected String name;

    protected boolean isAlive = true;
    protected AnimalKind kind = AnimalKind.UNKNOWN_FLYING_STUFF; //Nobody knows what it is... .
    protected float mass;

    protected int fatigueLevel = 0;
    protected int hungryLevel = 0;
    protected int isHungryFor = 0; // How many cycles the animal is hungry.
    protected Map<AnimalState, Integer> hungryLevelDeltas = new HashMap<>(); // hugryLevel changes depending on animal's state.
    protected Map<AnimalState, Integer> fatigueLevelDeltas = new HashMap<>();
    protected int starvationThreshold = Integer.MAX_VALUE; // if hungryLevel > theThreshold ==> the animal is hungry.
    protected int sleepThreshold = Integer.MAX_VALUE; // if fatigueLevel > the Threshold ==> the animal is sleeping.

    protected AnimalState state = AnimalState.DOES_NOTHING; //Initial state

    public Animal(String name){
        this.name = name;
        setInitialInternalState();
        setSleepThreshold();
        setStarvationThreshold();
        setHungryDeltas();
        setFatigueDeltas();
    }

    public abstract String getSpeciesName();

    /**
     * Max food items it can eat - need to calculate how much to give upon feeding.
     */
    public abstract int getMaxAmountOfFoodItCanEat();

    public boolean eat(int amountOfFood) {
        hungryLevel -= amountOfFood;
        if (0 > hungryLevel) {
            hungryLevel = 0; // Do not want for an animal to be over-fed.
        }

        //Food brings energy.
        fatigueLevel -= getFatigueDeltaUponState(AnimalState.DOES_NOTHING);
        logger.info(" <<<<<<< {}, ate {} units of food and is {}.",
                getTitle(), amountOfFood, (isHungry() ? "still hungry" : "happy now"));

        return isHungry();
    }

    /**
     * Each animal acts in its own way depending on the state.
     * Here is a basic/common way. Could and should be overridden by sub-class.
     */
    public void live() {
        if (isAlive()) {
            state = isSleeping() ? AnimalState.SLEEPS : nextState();

            logger.info("{} {}", getTitle(), getState());
            checkFatigueLevel();
            checkHungryLevel();

            //[x]REQ.80 If animal will be hungry (below some threshold) more than 24h it will leave Zoo.
            if (isHungryFor > MAX_COUNT_OF_CYCLES_BEFORE_LEAVE || mass < 0) {
                logger.warn("}}}}}}}}}}}}} Kirdyk {}", getTitle());
                isAlive = false;
            }

        }
    }

    public AnimalKind getKind() { return this.kind; }

    public String getName() { return this.name; }

    public String getTitle() {
        return getSpeciesName() + " " + getName();
    }

    public AnimalState getState() { return this.state; }

    public int getHungryLevel() { return this.hungryLevel; }

    public boolean isHungry() {
        return getHungryLevel() >= getStarvationThreshold();
    }

    public int getIsHungryFor() {
        return isHungryFor;
    }

    public int getFatigueLevel() { return this.fatigueLevel; }

    public boolean isAlive() {
        return this.isAlive;
    }

    public boolean isSleeping() {
        return getFatigueLevel() >= getSleepThreshold();
    }

    public float getMass() { return mass; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\n" + getSpeciesName());
        sb.append("{");
        sb.append("name='").append(name).append('\'');
        sb.append(", state=").append(state);
        sb.append(", fatigueLevel=").append(fatigueLevel);
        sb.append(", hungryLevel=").append(hungryLevel);
        sb.append(", hungry for=").append(isHungryFor);
        sb.append(", kind=").append(kind);
        sb.append(", mass=").append(mass);
        sb.append('}');
        return sb.toString();
    }

    /**
     * Each sub-class must provide a way to set internal state upon "birth".
     * The method is to be called in the abstract super class' constructor.
     */
    protected abstract void setInitialInternalState();

    protected abstract void setSleepThreshold();

    protected abstract void setFatigueDeltas();

    protected abstract void setStarvationThreshold();

    protected abstract void setHungryDeltas();

    protected int getSleepThreshold() { return sleepThreshold; }

    protected int getFatigueDeltaUponState(AnimalState state) {
        Integer result = fatigueLevelDeltas.get(state);

        if (null == result) {
            result = fatigueLevelDeltas.get(null);
        }

        return result;
    }

    protected int getStarvationThreshold() { return starvationThreshold; }

    protected int getHungryDeltaUponState(AnimalState state) {
        Integer result = hungryLevelDeltas.get(state);

        if (null == result) {
            result = hungryLevelDeltas.get(null);
        }

        return result;
    }

    /**
     * The animal decides what to do, so what would be the next state.
     *
     ** [x]REQ.45 Every hour animal need to decide what to do depending on internal state - search for food
     *  or sleep or do nothing.
     *
     *  Based on Gaussian distribution.
     *    /\
     *   /  \
     * /| | |\
     *  a b c
     *  where a - a fatigue threshold. If random value > abs(a) then the animal decides to sleep;
     *        c - a hungry threshold. If random value <= abs(c) then the animal decides to search for food;
     *        b - mean. No special action but if random value does not meet conditions a or c then the animal decides to do nothing;
     * Note: a and c are calculated basing on current fatigue and hungry level in such a way that
     *  1. Starvation increases probability of "go find something to eat".
     *  2. When tired - the animal tends to fall asleep.
     *
     * [x]REQ.60 Every animal will have own behaviour, which actions to do depending on internal state.
     *
     * @return - next state to switch into.
     */
    protected AnimalState nextState() {
        Random rnd = new Random();
        double value = rnd.nextGaussian();

        //Starvation increases probability of "go find something to eat".
        double hungryThreshold = (double) (getStarvationThreshold() - getHungryLevel() - isHungryFor)
                                / (double) (getStarvationThreshold() + getHungryLevel() + isHungryFor);

        // When tired - it tends to fall asleep.
        double fatigueThreshold = (double) (getSleepThreshold() + getFatigueLevel() - isHungryFor)
                                 / (double) (getSleepThreshold() - getFatigueLevel() + isHungryFor);

        if (Math.abs(value) >= hungryThreshold) {
            return AnimalState.SEARCHES_FOR_FOOD;
        } else if (Math.abs(value) < fatigueThreshold) {
            return AnimalState.SLEEPS;
        }

        // Most likely to select this one (as it corresponds to Gaussian mean value).
        return AnimalState.DOES_NOTHING;
    }

    private void checkHungryLevel() {
        int currentHungryLevelDelta = getHungryDeltaUponState(getState());
        hungryLevel += currentHungryLevelDelta;

        if (isHungry()) {
            isHungryFor++;
            mass -= Math.min(0.01 * mass, currentHungryLevelDelta / mass);
        }
    }

    private void checkFatigueLevel() {
        fatigueLevel += getFatigueDeltaUponState(getState());
        if (fatigueLevel < 0) { fatigueLevel = 0; }
    }
}
