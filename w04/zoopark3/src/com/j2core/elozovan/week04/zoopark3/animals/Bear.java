package com.j2core.elozovan.week04.zoopark3.animals;

import com.j2core.elozovan.week04.zoopark3.enums.AnimalKind;
import com.j2core.elozovan.week04.zoopark3.enums.AnimalState;

import java.util.Random;

/**
 * This is a Bear. Be polite.
 */
public class Bear extends Animal {

    public Bear(String name) {
        super(name);
    }

    @Override
    public int getMaxAmountOfFoodItCanEat() {
        return Math.round(getMass() / 15) + new Random().nextInt(Math.round(3 * getMass() / 4));
    }

    @Override
    public String getSpeciesName() { return "Bear"; }

    @Override
    protected void setInitialInternalState() {
        this.kind = AnimalKind.PREDATOR;
        Random rnd = new Random();
        this.mass = 10 + rnd.nextInt(400) + 5 * rnd.nextFloat();
    }

    @Override
    protected void setSleepThreshold() {
        sleepThreshold = 190 + new Random().nextInt(Math.round(getMass()));
    }

    @Override
    protected void setFatigueDeltas() {
        fatigueLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 5 + new Random().nextInt(getSleepThreshold()));
        fatigueLevelDeltas.put(AnimalState.SLEEPS,  -(3 + new Random().nextInt(getSleepThreshold()))); // Sleeping restores stamina.
        fatigueLevelDeltas.put(AnimalState.DOES_NOTHING, 2 + new Random().nextInt(getSleepThreshold()));
        fatigueLevelDeltas.put(null, 5 + new Random().nextInt(120));
    }

    @Override
    protected void setStarvationThreshold() {
        starvationThreshold = 390 + new Random().nextInt(Math.round(getMass()));
    }

    @Override
    protected void setHungryDeltas() {
        hungryLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 2 + new Random().nextInt(10 + Math.round(getMass() / 30)));
        hungryLevelDeltas.put(AnimalState.SLEEPS,  1 + new Random().nextInt(1 + Math.round(getMass() / 20)));
        hungryLevelDeltas.put(AnimalState.DOES_NOTHING, 2 + new Random().nextInt(2 + Math.round(getMass() / 10)));
        hungryLevelDeltas.put(null, 5 + new Random().nextInt(5 + Math.round(getMass())));
    }
}