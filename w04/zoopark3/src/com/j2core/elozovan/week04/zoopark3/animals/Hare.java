package com.j2core.elozovan.week04.zoopark3.animals;

import com.j2core.elozovan.week04.zoopark3.enums.AnimalKind;
import com.j2core.elozovan.week04.zoopark3.enums.AnimalState;

import java.util.Random;

/**
 * This is a Hare. Just A'Hare.
 */
public class Hare extends Animal {

    public Hare(String name) {
        super(name);
    }

    @Override
    public int getMaxAmountOfFoodItCanEat() {
        return Math.round(getMass()) + new Random().nextInt(getStarvationThreshold());
    }

    @Override
    public String getSpeciesName() { return "Hare"; }

    @Override
    protected void setInitialInternalState() {
        this.kind = AnimalKind.HERBIVOROUS;
        Random rnd = new Random();
        this.mass = 1 + rnd.nextInt(5) + rnd.nextFloat();
    }

    @Override
    protected void setSleepThreshold() {
        sleepThreshold = 40 + new Random().nextInt(12);
    }

    @Override
    protected void setFatigueDeltas() {
        fatigueLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 3 + new Random().nextInt(Math.round(getMass())));
        fatigueLevelDeltas.put(AnimalState.SLEEPS,  -(1 + new Random().nextInt(Math.round(getMass())))); // Sleeping restores stamina.
        fatigueLevelDeltas.put(AnimalState.DOES_NOTHING, 2 + new Random().nextInt(Math.round(getMass())));
        fatigueLevelDeltas.put(null, 1 + new Random().nextInt(12));
    }

    @Override
    protected void setStarvationThreshold() {
        starvationThreshold = 170 + new Random().nextInt(Math.round(getMass()));
    }

    @Override
    protected void setHungryDeltas() {
        hungryLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 2 + new Random().nextInt(2 + Math.round(getMass() / 2)));
        hungryLevelDeltas.put(AnimalState.SLEEPS,  1 + new Random().nextInt(1 + Math.round(getMass() / 2)));
        hungryLevelDeltas.put(AnimalState.DOES_NOTHING, 1 + new Random().nextInt(2 + Math.round(getMass() / 5)));
        hungryLevelDeltas.put(null, 5 + new Random().nextInt(5 + Math.round(getMass())));
    }
}
