package com.j2core.elozovan.week04.zoopark3.animals;

import com.j2core.elozovan.week04.zoopark3.enums.AnimalKind;
import com.j2core.elozovan.week04.zoopark3.enums.AnimalState;

import java.util.Random;

/**
 * This is a Wolf. A grey one.
 */
public class Wolf extends Animal {

    public Wolf(String name) {
        super(name);
    }

    @Override
    public int getMaxAmountOfFoodItCanEat() {
        return Math.round(getMass() / 7) + new Random().nextInt(Math.round(3 * getMass() / 4));
    }

    @Override
    public String getSpeciesName() { return "Wolf"; }

    @Override
    protected void setInitialInternalState() {
        this.kind = AnimalKind.PREDATOR;
        Random rnd = new Random();
        this.mass = 5 + rnd.nextInt(18) + 5 * rnd.nextFloat();
    }

    @Override
    protected void setSleepThreshold() {
        sleepThreshold = 65 + new Random().nextInt(Math.round(getMass()));
    }

    @Override
    protected void setFatigueDeltas() {
        fatigueLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 4 + new Random().nextInt(getSleepThreshold()));
        fatigueLevelDeltas.put(AnimalState.SLEEPS,  -(2 + new Random().nextInt(getSleepThreshold()))); // Sleeping restores stamina.
        fatigueLevelDeltas.put(AnimalState.DOES_NOTHING, 1 + new Random().nextInt(getSleepThreshold()));
        fatigueLevelDeltas.put(null, 5 + new Random().nextInt(42));
    }

    @Override
    protected void setStarvationThreshold() {
        starvationThreshold = 240 + new Random().nextInt(Math.round(getMass()));
    }

    @Override
    protected void setHungryDeltas() {
        hungryLevelDeltas.put(AnimalState.SEARCHES_FOR_FOOD, 2 + new Random().nextInt(10 + Math.round(getMass() / 20)));
        hungryLevelDeltas.put(AnimalState.SLEEPS,  1 + new Random().nextInt(3 + Math.round(getMass() / 20)));
        hungryLevelDeltas.put(AnimalState.DOES_NOTHING, 2 + new Random().nextInt(7 + Math.round(getMass() / 10)));
        hungryLevelDeltas.put(null, 5 + new Random().nextInt(5 + Math.round(getMass())));
    }
}