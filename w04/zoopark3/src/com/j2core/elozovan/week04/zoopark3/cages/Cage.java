package com.j2core.elozovan.week04.zoopark3.cages;

import com.j2core.elozovan.week04.zoopark3.animals.Animal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Represents a Cage.
 */
public class Cage {
    Logger logger = LoggerFactory.getLogger(Cage.class);

    private String name;
    private int foodAmount = 0;

    private List<Animal> animals = new CopyOnWriteArrayList<>();

    public Cage() {
        init("C-" + UUID.randomUUID().toString());
    }

    public Cage(String name) {
        init(name);
    }

    public String getName() {
        return name;
    }

    public List<Animal> getAnimals() {
        return this.animals;
    }

    public void addAnimal(Animal newAnimal) {
        logger.info("~Adding new animal into '{}': \n {}", name, newAnimal);
        animals.add(newAnimal);
    }

    public void letAllAnimalsToLeave() {
        letAnimalsToLeave(animals);
    }

    public void letAnimalsToLeave(List<Animal> animalsLeavingTheCage) {
        if (!animalsLeavingTheCage.isEmpty()) {
            logger.info("{} animal{} leaving {}.",
                    animalsLeavingTheCage.size(),
                    animalsLeavingTheCage.size() > 1 ? "s are" : " is",
                    name);
            animals.removeAll(animalsLeavingTheCage);
        }
    }

    public int getCurrentFoodAmount() {
        return foodAmount;
    }

    /**
     * The method mimics taking some food from the cage to feed animals.
     *
     * If amountNeed >= available food amount then the method
     * decreases foodAmount by amountNeeded and returns amountNeeded.
     *
     * otherwise sets foodAmount to 0 and returns its previous value.
     */
    public int deductFood(int amountNeed) {
        int toReturn = foodAmount;
        if (amountNeed <= foodAmount) {
            foodAmount -= amountNeed;
            toReturn = amountNeed;
        } else {
            foodAmount = 0;
        }

        return toReturn;
    }

    public void addFood(int amountToAdd) {
        foodAmount+=amountToAdd;

        logger.info("<--!--> ZooKeeper loves us! The caravan has just brought "
                + amountToAdd + " units of food into " + name
                + " and now we have " + foodAmount + " in total.");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nCage{");
        sb.append("name='").append(name).append('\'');
        sb.append(", foodAmount=").append(foodAmount);
        sb.append(", animals=").append(animals);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cage cage = (Cage) o;

        if (foodAmount != cage.foodAmount) return false;
        if (logger != null ? !logger.equals(cage.logger) : cage.logger != null) return false;
        return !(name != null ? !name.equals(cage.name) : cage.name != null) && !(animals != null ? !animals.equals(cage.animals) : cage.animals != null);
    }

    @Override
    public int hashCode() {
        int result = logger != null ? logger.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + foodAmount;
        result = 31 * result + (animals != null ? animals.hashCode() : 0);
        return result;
    }

    private void init(String name) {
        this.name = name;
        foodAmount = new Random().nextInt(303 + name.length());
        logger.info("Cage {}, initial amount of food - {} units.", this.name, this.foodAmount);
    }
}
