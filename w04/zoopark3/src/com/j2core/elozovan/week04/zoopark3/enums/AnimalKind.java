package com.j2core.elozovan.week04.zoopark3.enums;

public enum AnimalKind {
    PREDATOR,
    HERBIVOROUS,
    UNKNOWN_FLYING_STUFF
}
