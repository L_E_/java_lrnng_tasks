package com.j2core.elozovan.week04.zoopark3.enums;

public enum AnimalState {
    DOES_NOTHING,
    SLEEPS,
    SEARCHES_FOR_FOOD
}
