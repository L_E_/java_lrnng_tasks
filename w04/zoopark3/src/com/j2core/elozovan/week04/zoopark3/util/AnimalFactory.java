package com.j2core.elozovan.week04.zoopark3.util;

import com.j2core.elozovan.week04.zoopark3.animals.*;

import java.util.Random;

/**
 * Creates animals.
 */
public class AnimalFactory {

    public static Animal create(){
        return create(NameProvider.getANameForAnimal());
    }

    /**
     * Creates some animal.
     *
     * @return - an animal created.
     */
    public static Animal create(String nameForAnimal){

        if (null == nameForAnimal) {
            nameForAnimal = NameProvider.getANameForAnimal();
        }

        int flippedCoin = new Random().nextInt(10);

        switch (flippedCoin) {
            case 0:
            case 7:
            default:
                return new Bear(nameForAnimal);
            case 1:
            case 8:
                return new Hare(nameForAnimal);
            case 2:
            case 5:
                return new Fox(nameForAnimal);
            case 3:
            case 4:
                return new Wolf(nameForAnimal);
            case 6:
            case 9:
                return new Bear(nameForAnimal);
        }
    }
}
