package com.j2core.elozovan.week04.zoopark3.util;

import java.util.Random;
import java.util.UUID;

/**
 * An utility class which serves as a provider of names for animals, keepers etc.
 */
public class NameProvider {

    public static String getANameForAnimal() {
        String name;
        if (new Random().nextDouble() >= 0.2) {
            String[] candidates = getAnimalNameCandidates();
            int index = new Random().nextInt(candidates.length);
            name = candidates[index];
        } else {
            name = UUID.randomUUID().toString().replaceAll(" ", "").replaceAll("\\d", "");
        }

        return name;
    }

    public static String getANameForZooKeeper() {
        String name;
        if (new Random().nextDouble() >= 0.2) {
            String[] candidates = getZooKeeperNameCandidates();
            int index = new Random().nextInt(candidates.length);
            name = candidates[index];
        } else {
            name = "The Xranitel.";
        }

        return name;
    }

    private static String[] getAnimalNameCandidates() {
        return new String[]{"Umka", "Beluga", "Revjaka",
                            "Xrjak", "Brjak", "VseYak",
                            "Xudozhnik", "Zalipuha", "Dazdraperma",
                            "Krasnuha", "4u4a", "Vyhuholina",
                            "ShmYak", "Ziza", "Abvgdejka",
                            "Vypolzen'", "Planerjuga", "Podkustovnik",
                            "Bobo6ka'", "Kulja", "Pianyj gus'",
                            "Barsik'", "Uu", "Arbuzik",
                            "Zanuda"};
    }

    private static String[] getZooKeeperNameCandidates() {
        return new String[]{"Yaropolk", "Mordun", "Zverila",
                            "Xrjun", "Zubilo", "Morzhov",
                            "Petr", "Vasisualiy", "Lohankin",
                            "Kletkin", "Storozhilov"};
    }
}