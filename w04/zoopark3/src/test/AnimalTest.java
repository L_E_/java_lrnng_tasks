import com.j2core.elozovan.week04.zoopark3.util.AnimalFactory;
import com.j2core.elozovan.week04.zoopark3.animals.Animal;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AnimalTest {

    @Test
    public void testEat() throws Exception {
        Animal animal = AnimalFactory.create();

        for (int i = 0; i < 24; i++) {
            animal.live();
        }

        int initialLevel = animal.getHungryLevel();
        animal.eat(100500);

        assertTrue(animal.getHungryLevel() < initialLevel, animal.getSpeciesName() + " cannot eat.");
    }

    @Test
    public void testLive() throws Exception {
        Animal animal = AnimalFactory.create();

        for (int i = 0; i < 24; i++) {
            animal.live();
        }

        assertTrue(animal.isHungry(), animal.getSpeciesName() + " is not hungry after 23 cycles.");
        assertTrue(animal.getIsHungryFor() > 0, animal.getSpeciesName() + "'s 'isHungryFor' is <=0 after 23 cycles.");
        assertTrue(animal.getFatigueLevel() > 0, animal.getSpeciesName() + "'s fatigue level is <=0 after 23 cycles.");
    }

    @Test
    public void testGetMass() throws Exception {
        Animal animal = AnimalFactory.create();

        assertTrue(animal.getMass() > 0, animal.getSpeciesName() + " - does not have positive mass.");
    }

    @Test
    public void testGetMaxAmountOfFoodItCanEat() throws Exception {
        Animal animal = AnimalFactory.create();

        assertTrue(animal.getMaxAmountOfFoodItCanEat() > 0, animal.getSpeciesName() + " can eat 0 or less food maximum, that is wrong.");
    }
}