import com.j2core.elozovan.week04.zoopark3.util.AnimalFactory;
import com.j2core.elozovan.week04.zoopark3.cages.Cage;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CageTest {

    @Test
    public void testGetName() throws Exception {
        Cage cage = new Cage();

        assertTrue(null != cage.getName());
        assertTrue(cage.getName().length() > 0);

        String name = "n";
        cage = new Cage(name);

        assertEquals(cage.getName(), name);
    }

    @Test
    public void testGetCurrentFoodAmount() throws Exception {
        Cage cage = new Cage();

        assertTrue(cage.getCurrentFoodAmount() > 0);
    }

    @Test
    public void testDeductFood() throws Exception {
        Cage cage = new Cage();

        int before = cage.getCurrentFoodAmount();

        int returned = cage.deductFood(before - 1);
        assertEquals(returned, before - 1);
        assertEquals(cage.getCurrentFoodAmount(), 1);

        cage = new Cage();

        before = cage.getCurrentFoodAmount();

        returned = cage.deductFood(before + 1);
        assertEquals(returned, before);
        assertEquals(cage.getCurrentFoodAmount(), 0);
    }

    @Test
    public void testAddFood() throws Exception {
        Cage cage = new Cage();

        int before = cage.getCurrentFoodAmount();
        cage.addFood(144);
        assertEquals(cage.getCurrentFoodAmount(), before + 144);
    }

    @Test
    public void testAddAnimal() throws Exception {
        Cage cage = new Cage();

        assertEquals(cage.getAnimals().size(), 0);

        cage.addAnimal(AnimalFactory.create());
        assertEquals(cage.getAnimals().size(), 1);
    }

    @Test
    public void testRemoveAnimals() throws Exception {
        Cage cage = new Cage();
        cage.addAnimal(AnimalFactory.create());
        cage.letAllAnimalsToLeave();
        assertEquals(cage.getAnimals().size(), 0);
    }
}