import com.j2core.elozovan.week04.zoopark3.ZooPark;
import com.j2core.elozovan.week04.zoopark3.cages.Cage;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

public class ZooParkTest {

    @Test
    public void testHireZooKeeper() throws Exception {
        ZooPark zoo = new ZooPark();
        zoo.hireZooKeeper();

        assertTrue(null != zoo.getZooKeeper());
    }

    @Test
    public void testBuildCages() throws Exception {
        ZooPark zoo = new ZooPark();
        assertTrue(null == zoo.getCages());

        zoo.buildCages();

        assertTrue(zoo.getCages().size() > 0);
    }

    @Test
    public void testPopulateWithAnimals() throws Exception {
        ZooPark zoo = new ZooPark();
        zoo.buildCages();
        zoo.populateWithAnimals();

        for (Cage cage : zoo.getCages()) {
            assertTrue(cage.getAnimals().size() > 0);
        }
    }

    @Test
    public void testFunction() throws Exception {
        ZooPark zoo = new ZooPark();
        zoo.hireZooKeeper();
        zoo.buildCages();
        zoo.populateWithAnimals();

        List<Cage> before = new ArrayList<>();
        Collections.copy(zoo.getCages(), before);

        zoo.function();

        assertNotEquals(zoo.getCages(), before);
    }
}