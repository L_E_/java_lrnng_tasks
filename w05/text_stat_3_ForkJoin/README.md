Task:
You have to read text file, process every paragraph and write result in a single file.
Processing means that you'll calculate length, words count, average words length,
how many dots and commas you have and fingerprint (hash, SHA-1).
In the output file you have to write paragraph and additional info from processing.

====================================================================================

General idea.
Use combined seq/parallel approach: read and write sequentially, calculate statistic in parallel using ForkJoin task.

Details.
 InputReader -  reads data from file, forms data-entries-to-process(paragraphs)
and puts them into the provided List<String>. A data portion between empty lines is considered a paragraph.
See InputReader class.

 Stats calculation tasks (FJ Actions) - calculate stats recursively and store in-place(the input data List is re-used).
See CalculationThread and StatCalc classes.

 ResultWriter - writes data-entries into the result file. See ResultWriterThread class.

Statistician.java - an orchestrator for all the parts.
