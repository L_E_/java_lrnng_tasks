package com.j2core.elozovan.week05;

import com.j2core.elozovan.week05.calculation.CalculateStatsAction;
import com.j2core.elozovan.week05.io.InputReader;
import com.j2core.elozovan.week05.io.ResultWriter;
import com.j2core.elozovan.week05.util.PropertiesHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

/**
 * An orchestrator for all the parts.
 */
public class Statistician {
    static Logger logger = LoggerFactory.getLogger(Statistician.class);

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        List<String> data = new CopyOnWriteArrayList<>();

        // Read all data from file.
        new InputReader(PropertiesHolder.getInputFilePath(), data).run();

        // Calculate statistics recursively.
        ForkJoinPool fjPool = new ForkJoinPool();
        CalculateStatsAction calculateStatsAction = new CalculateStatsAction(
                                                            data,
                                                            0,
                                                            data.size());
        fjPool.invoke(calculateStatsAction);

        // Write calculated statistics to disk.
        new ResultWriter(data, PropertiesHolder.getResultFilePath()).run();

        long finish = System.currentTimeMillis();
        logger.info("|||||||||||||||||||||||||||||||||||||> It took {} ms.", finish - start);
    }
}