package com.j2core.elozovan.week05.calculation;

import com.j2core.elozovan.week05.data.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

/**
 * A ForkJoin task to recursively calculate text statistics.
 */
public class CalculateStatsAction extends RecursiveAction {
    Logger logger = LoggerFactory.getLogger(CalculateStatsAction.class);
    private static final int THRESHOLD = 512; //Static threshold for "stop-forking".
    private static final int DYN_THRESHOLD = 2; // Threshold for "stop-forking" to be used along with #getSurplusQueuedTaskCount().
    List<String> dataEntries;
    int start, end;

    public CalculateStatsAction(List<String> entries, int startIndex, int endIndex) {
        dataEntries = entries;
        start = startIndex;
        end = endIndex;
        logger.debug("Action: start={}; end={}", startIndex, endIndex);
    }

    @Override
    protected void compute() {
        // #getSurplusQueuedTaskCount() <== jeeconf.com/archive/jeeconf-2012/materials/fork-join/
        if ((end - start) < THRESHOLD || ForkJoinTask.getSurplusQueuedTaskCount() >= DYN_THRESHOLD) {
            if (logger.isDebugEnabled()) {
                logger.debug("Action: delta < threshold");
            }
            for (int i = start; i < end; i++) {
                String stats = calculateStats(dataEntries.get(i));
                dataEntries.set(i, stats);
            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Split again.");
            }
            int middle = (end + start) / 2;

            invokeAll(new CalculateStatsAction(dataEntries, start, middle),
                      new CalculateStatsAction(dataEntries, middle, end));
        }
    }

    private String calculateStats(String entry) {
        if (logger.isDebugEnabled()) {
            logger.debug("Processing text entry.");
        }
        StatsCalc calc = new StatsCalc();

        ResultEntity re = new ResultEntity();
        re.setEntry(entry);
        re.setFingerprint(calc.calculateFingerprint(entry));
        re.setWordsCount(calc.calculateWordsCount(entry));
        re.setAverageWordLength(calc.calculateAverageWordLength(entry));
        re.setDotsCount(calc.calculateDotsCount(entry));
        re.setCommasCount(calc.calculateCommasCount(entry));

        return re.toOutputRepresentation();
    }
}