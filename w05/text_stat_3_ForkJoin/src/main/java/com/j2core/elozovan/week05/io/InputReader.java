package com.j2core.elozovan.week05.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Scanner;

/**
 *  Input/Reader - runs in a  separate thread, reads data from file, forms data-entries-to-process(paragraphs)
 and puts them into the InputQueue. A data portion between empty lines is considered a paragraph.
 */
public class InputReader {
    private static Logger logger = LoggerFactory.getLogger(InputReader.class);

    private String pathToInput;
    private int entriesCount = 0;

    private List<String> data;

    public InputReader(String pathToInputFile, List<String> dataEntries) {
        this.pathToInput = pathToInputFile;
        this.data = dataEntries;
    }

    public void run() {
        logger.info("InputReader: Starting to get input data from {}.", pathToInput);
        Scanner scanner = new Scanner(getClass().getResourceAsStream(pathToInput));
        StringBuilder stringBuffer = new StringBuilder();

        while (scanner.hasNextLine()) {
            if (logger.isDebugEnabled()) {
                logger.debug(" ==> Reading an entry.");
            }
            String entry = scanner.nextLine();
            stringBuffer.append(entry);
            if (entry.isEmpty() || entry.equals("")) {
                stringBuffer = process(stringBuffer);
            }

        }
        process(stringBuffer); // Put the last portion.

        logger.info("[==>|X|] InputReader: Have read  {} entries from the file.", entriesCount);
    }

    private StringBuilder process(StringBuilder stringBuffer) {
        data.add(stringBuffer.toString());
        stringBuffer = new StringBuilder();
        entriesCount++;

        return stringBuffer;
    }
}