package com.j2core.elozovan.week05.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 *  ResultWriter - runs in a separate thread. As soon as there is data in the ResultQueue - starts to poll
 the data and write into the result file.
 */
public class ResultWriter {
    private static Logger logger = LoggerFactory.getLogger(ResultWriter.class);
    private String pathToOutput;
    List<String> dataEntries;

    public ResultWriter(List<String> entries, String pathToOutput) {
        this.dataEntries = entries;
        this.pathToOutput = pathToOutput;
    }

    public void run() {
        logger.info("ResultWriter: Starting result text entries processing.");

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                new FileOutputStream(pathToOutput), "utf-8"))) {
            for (String entry : dataEntries) {
                processEntry(entry, writer);
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Could not write to output file.", e);
        }

        logger.info("ResultWriter: Finished result  text entries processing.");
    }

    private void processEntry(String entry, Writer writer) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("ResultWriter: Processing result text entry.");
        }
        writer.write(entry);
        writer.write("\n=======================================");
    }
}
