package com.j2core.elozovan.week05.calculation;

import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ForkJoinPool;

import static org.testng.Assert.*;

public class CalculateStatsActionTest {

    @Test
    public void testCompute() throws Exception {
        List<String> data = new CopyOnWriteArrayList<>();

        data.add("Abv, gde.");

        ForkJoinPool fjPool = new ForkJoinPool();
        CalculateStatsAction calculateStatsAction = new CalculateStatsAction(
                data,
                0,
                data.size());
        fjPool.invoke(calculateStatsAction);

        String expected="\n" +
                "Abv, gde.\n" +
                "length=9, \n" +
                "wordsCount=2, \n" +
                "averageWordLength=3, \n" +
                "dotsCount=1, \n" +
                "commasCount=1, \n" +
                "fingerprint=6267429f6574f03d180c5c02767ef609d24099cf";
        assertEquals(expected, data.get(0));
    }
}