package com.j2core.elozovan.week05.calculation;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class StatsCalcTest {
    public static final String ENTRY = "321, 123, 456, 789.";

    @Test
    public void testCalculateDotsCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateDotsCount(ENTRY), 1);
        assertEquals(calc.calculateDotsCount(""), 0);
        assertEquals(calc.calculateDotsCount("... ."), 4);
    }

    @Test
    public void testCalculateCommasCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateCommasCount(ENTRY), 3);
        assertEquals(calc.calculateCommasCount(""), 0);
        assertEquals(calc.calculateCommasCount(",,, ."), 3);
    }

    @Test
    public void testCalculateFingerprint() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateFingerprint(ENTRY), "903e371645e741a8146745a245fc1eb24eedfc45");
        assertEquals(calc.calculateFingerprint(""), "da39a3ee5e6b4b0d3255bfef95601890afd80709");
    }

    @Test
    public void testCalculateWordsCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateWordsCount(ENTRY), 4);
        assertEquals(calc.calculateWordsCount(""), 0);
    }

    @Test
    public void testCalculateAverageWordLength() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateAverageWordLength(ENTRY), 3);
        assertEquals(calc.calculateAverageWordLength(""), 0);
    }
}