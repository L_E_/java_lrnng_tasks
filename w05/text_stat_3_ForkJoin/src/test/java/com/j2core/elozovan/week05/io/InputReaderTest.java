package com.j2core.elozovan.week05.io;

import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.testng.Assert.*;

public class InputReaderTest {

    @Test
    public void testRun() throws Exception {
        List<String> data = new CopyOnWriteArrayList<>();

        // Read all data from file.
        new InputReader("/input.txt", data).run();

        assertEquals(data.size(), 3);
        assertEquals(data.get(0), "Abzats 1.");
        assertEquals(data.get(1), "Ab, za ts 2.");
        assertEquals(data.get(2), "A, b, za t. s 3.");
    }
}