Task:
You have to read text file, process every paragraph and write result in a single file.
Processing means that you'll calculate length, words count, average words length,
how many dots and commas you have and fingerprint (hash, SHA-1).
In the output file you have to write paragraph and additional info from processing.

====================================================================================

General idea.
_________        ______________     ____________    ______________     ________
| Input  |       | DataPipeline|    | Stats calc|   | DataPipeline|    | Result|
| /Reader|       | InputQueue  |    | threads   |   | ResultQueue |    | Writer|
     |                  |               |                  |               |
 First entry -----put-->|           Waiting for IQ         |               |
     |                  |-------poll--->|                  |               |
    ... -------put----->|               |--------put------>|               |
  All entries           |               |                  |-------poll--->|
     |                  |-------poll-->... ------put------>|               |
  [Stop]                            All processed         ...-----poll---->|
                                        |                  |               |
                                     [Stop]                             [Stop]


Details.
 Input/Reader - runs in a  separate thread, reads data from file, forms data-entries-to-process(paragraphs)
and puts them into the InputQueue. For now a data portion between EOLs is considered a paragraph,
except empty lines. See InputReaderThread class.

 DataPipeline/InputQueue - a blocking queue containing not-yet processed chunks of data (paragraphs). Usage of blocking
queue allows to limit memory consumption. See DataPipeline class.

 Stats calculation thread(s) - wait(s) for data in the InputQueue then  start(s) to poll and process it.
Calculated stats are put into the ResultQueue. The thread(s) stop(s) running as soon as InputReader
is not alive and there is no data in the InputQueue. See CalculationThread and StatCalc classes.

 DataPipeline/ResultQueue - a blocking queue containing chunks of statistical data. Usage of blocking queue allows to
limit memory consumption.

 ResultWriter - runs in a separate thread. As soon as there is data in the ResultQueue - starts to poll
the data and write into the result file. See ResultWriterThread class.

Statistician.java - an orchestrator for all the parts.
