package com.j2core.elozovan.week05;

import com.j2core.elozovan.week05.calculation.CalculationThread;
import com.j2core.elozovan.week05.io.InputReaderThread;
import com.j2core.elozovan.week05.io.ResultWriterThread;
import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.util.PropertiesHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * An orchestrator for all the parts.
 */
public class Statistician {
    static Logger logger = LoggerFactory.getLogger(Statistician.class);
    //TODO: Make the value dependent on input file size.
    public static final int NUMBER_OF_TEXT_PROCESSOR_THREADS = 20;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        //TODO: Make the qs' size dependent on input file size.
        DataPipeline.setQSizes(64, 64);
        Thread inputProcessor = new InputReaderThread("Input processor",
                                            PropertiesHolder.get().getProperties().getProperty("input.file.path"),
                                            DataPipeline.get());

        List<Thread> textProcessors = new ArrayList<>(NUMBER_OF_TEXT_PROCESSOR_THREADS);

        for (int i=0; i < NUMBER_OF_TEXT_PROCESSOR_THREADS; i++) {
            textProcessors.add(new CalculationThread("Stats calculator-" + i, inputProcessor, DataPipeline.get()));
        }

        ExecutorService textProcessorsPool = Executors.newFixedThreadPool(NUMBER_OF_TEXT_PROCESSOR_THREADS);
        Thread resultProcessor = new ResultWriterThread("Result processor",
                                            textProcessorsPool,
                                            DataPipeline.get(),
                                            PropertiesHolder.get().getProperties().getProperty("result.file.path"));

        inputProcessor.start();
        resultProcessor.start();

        for (Thread thread : textProcessors) {
            textProcessorsPool.execute(thread);
        }

        try {
            inputProcessor.join();
            textProcessorsPool.shutdown();
            //TODO: Make the timeout dependent on input file size.
            textProcessorsPool.awaitTermination(32, TimeUnit.SECONDS);
            resultProcessor.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long finish = System.currentTimeMillis();
        logger.info("|||||||||||||||||||||||||||||||||||||> It took {} ms.", finish - start);
        logger.info("Stats (left/total processed): {}", DataPipeline.get().getStats() );
    }
}