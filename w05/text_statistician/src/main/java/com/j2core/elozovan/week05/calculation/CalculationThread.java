package com.j2core.elozovan.week05.calculation;

import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.data.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 Stats calculation thread(s) - wait for Input/Reader thread to become alive and as soon as there is data
 in the InputQueue start to poll and process them. Calculated stats are put into the ResultQueue. The thread(s)
 stop(s) running as soon as InputReader is not alive and there is no data in the InputQueue.
 */
public class CalculationThread extends Thread {
    static Logger logger = LoggerFactory.getLogger(CalculationThread.class);
    private Thread input;
    private DataPipeline dataPipeline;

    public CalculationThread(String name, Thread input, DataPipeline dataPipeline) {
        super(name);
        this.input = input;
        this.dataPipeline = dataPipeline;
    }

    public void run() {
        logger.info("{}: Starting text entries processing.", this.getName());

        while (input.isAlive() || (!input.isAlive() && !dataPipeline.isInputQueueEmpty())) {
            if (logger.isDebugEnabled()) {
                logger.debug("{}: {}.", this.getName(), dataPipeline.getStats());
            }

            String tmp = dataPipeline.pollInputEntry(1, TimeUnit.SECONDS);
            if (null != tmp) {
                processEntry(tmp);
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("{}: IQ is empty, finishing. {}.", this.getName(), dataPipeline.getStats());
                }
                break;
            }

            if (logger.isDebugEnabled()) {
                logger.debug("{}: after {}.", this.getName(), dataPipeline.getStats());
            }
        }

        logger.info("{}: [!] Finished text entries processing. {}", this.getName(), dataPipeline.getStats());
    }

    private void processEntry(String entry) {
        if (logger.isDebugEnabled()) {
            logger.debug("{}: Processing text entry.", this.getName());
        }
        StatsCalc calc = new StatsCalc(); //Shall be thread-safe this way.

        ResultEntity re = new ResultEntity();
        re.setEntry(entry);
        re.setFingerprint(calc.calculateFingerprint(entry));
        re.setWordsCount(calc.calculateWordsCount(entry));
        re.setAverageWordLength(calc.calculateAverageWordLength(entry));
        re.setDotsCount(calc.calculateDotsCount(entry));
        re.setCommasCount(calc.calculateCommasCount(entry));

        dataPipeline.putResultEntry(re);
        if (logger.isDebugEnabled()) {
            logger.debug("{}:   Processed text entry.", this.getName());
        }
    }
}