package com.j2core.elozovan.week05.calculation;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.hash.Hashing;

import java.util.List;

/**
 * A helper class which calculates test statistics.
 */
public class StatsCalc {

    public int calculateDotsCount(String entry){
        return getLengthDiffAfterRemovingSubstring(entry, "\\.");
    }

    public int calculateCommasCount(String entry){
        return getLengthDiffAfterRemovingSubstring(entry, ",");
    }

    public String calculateFingerprint(String entry) {
        return Hashing.sha1().hashString(entry, Charsets.UTF_8).toString();
    }

    public int calculateWordsCount(String entry) {
        return getListOfWords(entry).size();
    }

    public int calculateAverageWordLength(String entry) {
        List<String> words = getListOfWords(entry);
        long sum = 0;

        for (String word : words) {
            sum += word.length();
        }

        int size = words.size();
        return size != 0 ? (int) (sum / words.size()) : 0;
    }

    private List<String> getListOfWords(String entry) {
        return Splitter
                .onPattern("([,.]|\\s)")
                .trimResults()
                .omitEmptyStrings()
                .splitToList(entry);
    }

    private int getLengthDiffAfterRemovingSubstring(String entry, String subStringToRemove) {
        int before = entry.length();

        String tmp = entry.replaceAll(subStringToRemove, "");

        return before - tmp.length();
    }
}