package com.j2core.elozovan.week05.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Data pipeline.
 * Contains Input and Result blocking queues and allows
 * to put/get data into/from them.
 */
public class DataPipeline {
    static Logger logger = LoggerFactory.getLogger(DataPipeline.class);

    public static final int DEFAULT_INPUT_Q_SIZE = 64; //Some default value.
    public static final int DEFAULT_RESULT_Q_SIZE = 32; //Some default value.

    // These values are set only once, so they should be ThreadSafe.
    public static boolean isQSizesSet = false;
    public static int input_Q_size;
    public static int result_Q_size;

    private static volatile DataPipeline instance;
    private AtomicInteger inputCount = new AtomicInteger(0);
    private AtomicInteger resultCount = new AtomicInteger(0);
    private volatile BlockingQueue<String> inputData;
    private volatile BlockingQueue<ResultEntity> resultData;

    private DataPipeline() {
        if (!isQSizesSet) {
            input_Q_size = DEFAULT_INPUT_Q_SIZE;
            result_Q_size = DEFAULT_RESULT_Q_SIZE;
            isQSizesSet = true;
        }

        this.inputData = new LinkedBlockingQueue<>(input_Q_size);
        this.resultData = new LinkedBlockingQueue<>(result_Q_size);
    }

    public static DataPipeline get() {
        DataPipeline localInstance = instance;
        if (localInstance == null) {
            synchronized (DataPipeline.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataPipeline();
                }
            }
        }

        return localInstance;
    }

    public static void setQSizes(int inputQSize, int resultQSize) {
        input_Q_size = inputQSize;
        result_Q_size = resultQSize;
        isQSizesSet = true;
    }

    public boolean isInputQueueEmpty() {
        return inputData.isEmpty();
    }

    public BlockingQueue<String> getInputData() {
        return inputData;
    }

    public String pollInputEntry(long timeout, TimeUnit unit) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("<-- Polling an input entry away.");
            }
            return inputData.poll(timeout, unit);
        } catch (InterruptedException e) {
            logger.warn("Could not get input entry due to the interruption.", e);
        }

        return null;
    }

    public void putInputEntry(String newEntry) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("[\\/] Putting new input entry.");
            }
            inputData.put(newEntry);
            inputCount.getAndIncrement();
        } catch (InterruptedException e) {
            logger.warn("Could not put input entry due to the interruption.", e);
        }
    }

    public BlockingQueue<ResultEntity> getResultData() {
        return resultData;
    }

    public boolean isResultQueueEmpty() {
        return resultData.isEmpty();
    }

    public ResultEntity pollResultEntry(long timeout, TimeUnit unit) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("<--- Polling a result entry out of here.");
            }
            return resultData.poll(timeout, unit);
        } catch (InterruptedException e) {
            logger.warn("Could not get result entry due to the interruption.", e);
        }

        return null;
    }

    public void putResultEntry(ResultEntity newEntry) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("[\\/] Putting new result entry.");
            }
            resultData.put(newEntry);
            resultCount.getAndIncrement();
        } catch (InterruptedException e) {
            logger.warn("Could not put result entry due to the interruption.", e);
        }
    }

    /**
     * Mainly for debug purposes.
     * Returns current number of entries in both queues (input - IQ and result one - RQ).
     * Additionally shows total count of queued items i.e.
     * [IQ]=5/120 | [RQ]=20/40
     * means that IQ has currently 5 entries and 120 entries have been queued so far.
     */
    public String getStats() {
        return "[IQ]=" + inputData.size() + "/" + inputCount.intValue()
                + " | [RQ]=" + resultData.size() + "/" + resultCount.intValue();
    }
}