package com.j2core.elozovan.week05.io;

import com.j2core.elozovan.week05.data.DataPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

/**
 *  Input/Reader - runs in a  separate thread, reads data from file, forms data-entries-to-process(paragraphs)
 and puts them into the InputQueue. For now a data portion between EOLs is considered a paragraph,
 except empty lines.
 */
public class InputReaderThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(InputReaderThread.class);

    private String pathToInput;
    private volatile int entriesCount = 0;

    private DataPipeline dataPipeline;

    public InputReaderThread(String name, String pathToInputFile, DataPipeline dataPipeline) {
        super(name);
        this.pathToInput = pathToInputFile;
        this.dataPipeline = dataPipeline;
    }

    @Override
    public void run() {
        logger.info("{}: Starting to get input data from {}.", this.getName(), pathToInput);
        //TODO: Might need to use Spring to access for resource files.
        Scanner scanner = new Scanner(getClass().getResourceAsStream(pathToInput));

        while (scanner.hasNextLine()) {
            if (logger.isDebugEnabled()) {
                logger.debug(" ==> Reading an entry.");
            }
            String entry = scanner.nextLine();

            if (!(entry.isEmpty() || entry.equals(""))) {
                dataPipeline.putInputEntry(entry);
                entriesCount++;
            }
        }

        logger.info("[==>|X|] Have read all the {} lines from the file.", entriesCount);
    }
}