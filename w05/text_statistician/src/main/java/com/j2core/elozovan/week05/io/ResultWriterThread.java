package com.j2core.elozovan.week05.io;

import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.data.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *  ResultWriter - runs in a separate thread. As soon as there is data in the ResultQueue - starts to poll
 the data and write into the result file.
 */
public class ResultWriterThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(ResultWriterThread.class);
    private String pathToOutput;
    private DataPipeline dataPipeline;

    private ExecutorService calculationThreadsPool;

    public ResultWriterThread(String name, ExecutorService pool, DataPipeline dataPipeline, String pathToOutput) {
        super(name);
        this.calculationThreadsPool = pool;
        this.dataPipeline = dataPipeline;
        this.pathToOutput = pathToOutput;
    }

    public void run() {
        logger.info("{}:Starting result text entries processing.", this.getName());

        while (dataPipeline.isResultQueueEmpty()) {
            Thread.yield();
        }

        logger.info("{}: Got some data in the result queue, starting actual processing.", this.getName());
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                new FileOutputStream(pathToOutput), "utf-8"))) {

            while (!calculationThreadsPool.isTerminated()
                    ||
                    (calculationThreadsPool.isTerminated() && !dataPipeline.isResultQueueEmpty())) {
                if (logger.isDebugEnabled()) {
                    logger.debug("{} is about to process result entry.", this.getName());
                    logger.debug("{}: before - {}.", this.getName(), dataPipeline.getStats());
                }
                ResultEntity tmp = dataPipeline.pollResultEntry(1, TimeUnit.SECONDS);
                if (null != tmp) {
                    processEntry(tmp, writer);
                } else {
                    break;
                }

                if (logger.isDebugEnabled()) {
                    logger.debug("{}: after - {}.", this.getName(), dataPipeline.getStats());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Could not write to output file.", e);
        }

        logger.info("{}: Finished result  text entries processing.", this.getName());
    }

    private void processEntry(ResultEntity entry, Writer writer) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("{}: Processing result text entry.", this.getName());
        }
        writer.write(entry.toOutputRepresentation());
        writer.write("\n=======================================");
    }
}
