package com.j2core.elozovan.week05.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesHolder {
    private static volatile PropertiesHolder instance;
    private volatile Properties properties;

    private PropertiesHolder(){
        this.properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/app.properties"));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Taken from https://ru.wikipedia.org/wiki/%D0%9E%D0%B4%D0%B8%D0%BD%D0%BE%D1%87%D0%BA%D0%B0_%28%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%29#.D0.9F.D1.80.D0.B8.D0.BC.D0.B5.D1.80_.D0.BD.D0.B0_Java_1.6:_.D0.91.D0.B5.D0.B7_.D0.B2.D0.BD.D1.83.D1.82.D1.80.D0.B5.D0.BD.D0.BD.D0.B8.D1.85_.D0.BA.D0.BB.D0.B0.D1.81.D1.81.D0.BE.D0.B2_.28.D0.BB.D0.B5.D0.BD.D0.B8.D0.B2.D0.B0.D1.8F_.D1.81.D0.B8.D0.BD.D1.85.D1.80.D0.BE.D0.BD.D0.B8.D0.B7.D0.B8.D1.80.D0.BE.D0.B2.D0.B0.D0.BD.D0.BD.D0.B0.D1.8F_.D1.80.D0.B5.D0.B0.D0.BB.D0.B8.D0.B7.D0.B0.D1.86.D0.B8.D1.8F.29.
    public static PropertiesHolder get() {
        PropertiesHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (PropertiesHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PropertiesHolder();
                }
            }
        }

        return localInstance;
    }

    public Properties getProperties() { return properties; }
}