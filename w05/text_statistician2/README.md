Task:
You have to read text file, process every paragraph and write result in a single file.
Processing means that you'll calculate length, words count, average words length,
how many dots and commas you have and fingerprint (hash, SHA-1).
In the output file you have to write paragraph and additional info from processing.

====================================================================================

General idea.
Use Input, Calculation, Writer threads and synchronise them using Phaser.
_________        ______________     ____________    ______________     ________
| Input  |       | DataPipeline|    | Stats calc|   | DataPipeline|    | Result|
| /Reader|       | InputQueue  |    | threads   |   | ResultQueue |    | Writer|
     |                  |               |                  |               |
 First entry -----put-->|           Waiting for IQ         |               |
     |---> To Phase-1   |-------poll--->|                  |               |
    ... -------put----->|               |--------put------>|               |
  All entries           |               |--> To Phase-2    |-------poll--->|
     |---> To Phase-2   |-------poll-->... ------put------>|               |
  [Stop]                            All processed         ...-----poll---->|
                                        |--> To Phase-3    |               |
                                     [Stop]                             [Stop]

Details.
 Input/Reader - runs in a  separate thread, reads data from file, forms data-entries-to-process(paragraphs)
and puts them into the InputQueue.
Note, as soon as first entry has been put the Reader calls phasers's#arrive() thus advances to the next phase.
When all entries are read then the Reader calls phaser's #arriveAndDeregister() -> the phaser goes to the next
phase.
Also, a data portion between empty lines is considered a paragraph. See InputReaderThread class.

 DataPipeline/InputQueue - a blocking queue containing not-yet processed chunks of data (paragraphs). Usage of blocking
queue allows to limit memory consumption. See DataPipeline class.

 Stats calculation thread(s) - wait(s) for data in the InputQueue (by calling phaser's #arriveAndAwaitAdvance()
i.e. waits for phase-1 which means InputReader has put the very first entry into the queue) then  start(s) to
poll and process it. Calculated stats are put into the ResultQueue. The thread(s) stop(s) running as soon as
there is no data in the InputQueue. See CalculationThread and StatCalc classes.

 DataPipeline/ResultQueue - a blocking queue containing chunks of statistical data. Usage of blocking queue allows to
limit memory consumption.

 ResultWriter - runs in a separate thread. As soon as there is data in the ResultQueue - starts to poll
the data and write into the result file. See ResultWriterThread class.

Statistician.java - an orchestrator for all the parts.
