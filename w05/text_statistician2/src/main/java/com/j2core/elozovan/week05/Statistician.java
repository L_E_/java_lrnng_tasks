package com.j2core.elozovan.week05;

import com.j2core.elozovan.week05.calculation.CalculationThread;
import com.j2core.elozovan.week05.io.InputReaderThread;
import com.j2core.elozovan.week05.io.ResultWriter;
import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.util.PropertiesHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * An orchestrator for all the parts.
 * Uses Phaser to sync InputReader, Text processing and Result Writer parts.
 */
public class Statistician {
    static Logger logger = LoggerFactory.getLogger(Statistician.class);
    //TODO: Make the value dependent on input file size.
    public static final int NUMBER_OF_TEXT_PROCESSOR_THREADS = 20;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        //TODO: Make the qs' size dependent on input file size and available RAM.
        DataPipeline.setQSizes(64, 64);
        // N_of_text_processor threads + N_of_input_reader_threads (1 for now).
        Phaser phaser = new Phaser(1 + NUMBER_OF_TEXT_PROCESSOR_THREADS);

        // A new thread is started by the constructor.
        new InputReaderThread("Input processor",
                PropertiesHolder.get().getProperties().getProperty("input.file.path"),
                DataPipeline.get(),
                phaser);

        for (int i = 0; i < NUMBER_OF_TEXT_PROCESSOR_THREADS; i++) {
            // A new Text processor(Stats calculation) thread is started by the constructor.
            new CalculationThread("Stats calculator-" + i, DataPipeline.get(), phaser);
        }

        // Just a runnable, to use simple thread.join for sync below.
        Runnable resultWriter = new ResultWriter("Result processor",
                                            DataPipeline.get(),
                                            PropertiesHolder.get().getProperties().getProperty("result.file.path"),
                                            phaser);

        Thread resultWriterThread = new Thread(resultWriter);
        resultWriterThread.start();

        try {
            //Wait till input and calculation are done.
            while (!phaser.isTerminated()) { Thread.yield(); }
            //Wait till results are stored.
            resultWriterThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long finish = System.currentTimeMillis();
        logger.info("|||||||||||||||||||||||||||||||||||||> It took {} ms.", finish - start);
        logger.info("Pipeline stats (entries left/total processed): {}", DataPipeline.get().getStats() ); //Should be 0/N.
    }
}