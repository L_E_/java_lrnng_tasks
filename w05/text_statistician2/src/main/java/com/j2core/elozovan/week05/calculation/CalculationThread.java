package com.j2core.elozovan.week05.calculation;

import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.data.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 Stats calculation thread(s) - wait for Input/Reader thread and as soon as there is data
 in the InputQueue start to poll and process them. Calculated stats are put into the ResultQueue. The thread(s)
 stop(s) running as soon as there is no data in the InputQueue.
 */
public class CalculationThread implements Runnable {
    static Logger logger = LoggerFactory.getLogger(CalculationThread.class);
    private DataPipeline dataPipeline;
    private Phaser phaser;
    private InheritableThreadLocal<String> name = new InheritableThreadLocal<>();
    private InheritableThreadLocal<Boolean> hasStartedProcessing = new InheritableThreadLocal<>();

    public CalculationThread(String name, DataPipeline dataPipeline, Phaser phaser) {
        this.dataPipeline = dataPipeline;
        this.phaser = phaser;
        this.name.set(name);
        this.hasStartedProcessing.set(false);

        new Thread(this, name).start();
    }

    public void run() {
        logger.info("{}: Starting text entries processing.", name.get());
        phaser.arriveAndAwaitAdvance(); // Waiting for InputReader thread to put some data into IQ. If so - the phaser advances to the next phase.

        logger.info("{}: Seems there is some data in the Input Queue, starting actual processing.", name.get());

        String entry;
        while (null != (entry = dataPipeline.pollInputEntry(500, TimeUnit.MILLISECONDS))) {
            if (logger.isDebugEnabled()) {
                logger.debug("{}: before {}.", name.get(), dataPipeline.getStats());
            }

            processEntry(entry);

            // Put some data into RQ, so let's move to the next phase (let ResultWriter know it can start).
            if (!hasStartedProcessing.get()) {
                hasStartedProcessing.set(true);
                phaser.arrive();

                if (logger.isDebugEnabled()) {
                    logger.debug("{}: Phaser after 1-st calc's arrive = {} ", name.get(), phaser);
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("{}: after {}.", name.get(), dataPipeline.getStats());
            }
        }

        logger.info("{}: [!] Finished text entries processing. {}", name.get(), dataPipeline.getStats());

        // Handle the case when this thread is not lucky enough and there is no input data anymore.
        phaser.arriveAndDeregister();
        if (logger.isDebugEnabled()) {
            logger.debug("{}: Phaser = {}", name.get(), phaser);
        }
    }

    private void processEntry(String entry) {
        if (logger.isDebugEnabled()) {
            logger.debug("{}: Processing text entry.", name.get());
        }
        StatsCalc calc = new StatsCalc(); //Shall be thread-safe this way.

        ResultEntity re = new ResultEntity();
        re.setEntry(entry);
        re.setFingerprint(calc.calculateFingerprint(entry));
        re.setWordsCount(calc.calculateWordsCount(entry));
        re.setAverageWordLength(calc.calculateAverageWordLength(entry));
        re.setDotsCount(calc.calculateDotsCount(entry));
        re.setCommasCount(calc.calculateCommasCount(entry));

        dataPipeline.putResultEntry(re);
        if (logger.isDebugEnabled()) {
            logger.debug("{}:   Processed text entry.", name.get());
        }
    }
}