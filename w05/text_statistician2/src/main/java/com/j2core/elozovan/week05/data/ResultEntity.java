package com.j2core.elozovan.week05.data;

/**
 * Result container:
 * processed entry itself,
 * words/dots/comas count,
 * averageWords lengths and the entry's fingerprint.
 */
public class ResultEntity {
    public String entry;
    public int wordsCount;
    public int averageWordLength;
    public int dotsCount;
    public int commasCount;
    public String fingerprint;

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public int getLength() {
        return entry.length();
    }

    public int getWordsCount() {
        return wordsCount;
    }

    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }

    public int getAverageWordLength() {
        return averageWordLength;
    }

    public void setAverageWordLength(int averageWordLength) {
        this.averageWordLength = averageWordLength;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public int getDotsCount() {
        return dotsCount;
    }

    public void setDotsCount(int dotsCount) {
        this.dotsCount = dotsCount;
    }

    public int getCommasCount() {
        return commasCount;
    }

    public void setCommasCount(int commasCount) {
        this.commasCount = commasCount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResultEntity{");
        sb.append("entry='").append(entry).append('\'');
        sb.append(", wordsCount=").append(wordsCount);
        sb.append(", averageWordLength=").append(averageWordLength);
        sb.append(", dotsCount=").append(dotsCount);
        sb.append(", commasCount=").append(commasCount);
        sb.append(", fingerprint='").append(fingerprint).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String toOutputRepresentation() {
        final StringBuilder sb = new StringBuilder("\n");
        sb.append(entry);
        sb.append("\nlength=").append(getLength());
        sb.append(", \nwordsCount=").append(wordsCount);
        sb.append(", \naverageWordLength=").append(averageWordLength);
        sb.append(", \ndotsCount=").append(dotsCount);
        sb.append(", \ncommasCount=").append(commasCount);
        sb.append(", \nfingerprint=").append(fingerprint);
        return sb.toString();
    }
}