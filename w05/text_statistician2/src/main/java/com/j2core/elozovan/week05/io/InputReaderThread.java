package com.j2core.elozovan.week05.io;

import com.j2core.elozovan.week05.data.DataPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;
import java.util.concurrent.Phaser;

/**
 *  Input/Reader - runs in a  separate thread, reads data from file, forms data-entries-to-process(paragraphs)
 and puts them into the InputQueue. A data portion between empty lines is considered a paragraph.
 */
public class InputReaderThread implements  Runnable {
    private static Logger logger = LoggerFactory.getLogger(InputReaderThread.class);

    private String pathToInput;
    private InheritableThreadLocal<Integer> entriesCount = new InheritableThreadLocal<>();

    private DataPipeline dataPipeline;
    private Phaser phaser;
    private InheritableThreadLocal<Boolean> hasStartedReading = new InheritableThreadLocal<>();
    private InheritableThreadLocal<String> name = new InheritableThreadLocal<>();

    public InputReaderThread(String name, String pathToInputFile, DataPipeline dataPipeline, Phaser phaser) {
        this.pathToInput = pathToInputFile;
        this.dataPipeline = dataPipeline;
        this.phaser = phaser;
        this.hasStartedReading.set(false);
        this.name.set(name);
        entriesCount.set(0);

        new Thread(this, name).start();
    }

    @Override
    public void run() {
        logger.info("{}: Starting to get input data from {}.", name.get(), pathToInput);
        Scanner scanner = new Scanner(getClass().getResourceAsStream(pathToInput));
        StringBuilder stringBuffer = new StringBuilder();

        while (scanner.hasNextLine()) {
            if (logger.isDebugEnabled()) {
                logger.debug(" ==> Reading an entry.");
            }
            String entry = scanner.nextLine();
            stringBuffer.append(entry);
            if (entry.isEmpty() || entry.equals("")) {
                stringBuffer = process(stringBuffer);
            }

        }
        process(stringBuffer); // Put the last portion.

        logger.info("[==>|X|] {}: Have read  {} entries from the file.", name.get(), entriesCount.get());
        phaser.arriveAndDeregister();
    }

    private StringBuilder process(StringBuilder stringBuffer) {
        if (!hasStartedReading.get()) {
            hasStartedReading.set(true);
            phaser.arrive();
            if (logger.isDebugEnabled()) {
                logger.debug("Phaser after 1-st input's arrive = {} ", phaser);
            }
        }
        dataPipeline.putInputEntry(stringBuffer.toString());
        stringBuffer = new StringBuilder();
        entriesCount.set(entriesCount.get() + 1);

        return stringBuffer;
    }
}