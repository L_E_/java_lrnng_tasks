package com.j2core.elozovan.week05.io;

import com.j2core.elozovan.week05.data.DataPipeline;
import com.j2core.elozovan.week05.data.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *  ResultWriter - runs in a separate thread. As soon as there is data in the ResultQueue - starts to poll
 the data and write into the result file.
 */
public class ResultWriter implements Runnable {
    private static Logger logger = LoggerFactory.getLogger(ResultWriter.class);
    private String pathToOutput;
    private DataPipeline dataPipeline;
    private Phaser phaser;
    private InheritableThreadLocal<String> name = new InheritableThreadLocal<>();

    public ResultWriter(String name, DataPipeline dataPipeline, String pathToOutput, Phaser phaser) {
        this.dataPipeline = dataPipeline;
        this.pathToOutput = pathToOutput;
        this.phaser = phaser;
        this.name.set(name);
    }

    public void run() {
        logger.info("{}: Starting result text entries processing.", name.get());

        while (dataPipeline.isResultQueueEmpty()) {
            Thread.yield();
        }

        logger.info("{}: Got some data in the result queue, starting actual processing.", name.get());
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                new FileOutputStream(pathToOutput), "utf-8"))) {

            while ((!dataPipeline.isResultQueueEmpty() && phaser.isTerminated()) || !phaser.isTerminated()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("{} is about to process result entry.", name.get());
                    logger.debug("{}: before - {}.", name.get(), dataPipeline.getStats());
                }
                ResultEntity tmp = dataPipeline.pollResultEntry(1, TimeUnit.SECONDS);
                if (null != tmp) {
                    processEntry(tmp, writer);
                } else {
                    logger.debug("{}: !!! Got null upon polling the Result Queue.", name.get());
                    break;
                }

                if (logger.isDebugEnabled()) {
                    logger.debug("{}: after - {}.", name.get(), dataPipeline.getStats());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Could not write to output file.", e);
        }

        logger.info("{}: Finished result  text entries processing.", name.get());
    }

    private void processEntry(ResultEntity entry, Writer writer) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("{}: Processing result text entry.", name.get());
        }
        writer.write(entry.toOutputRepresentation());
        writer.write("\n=======================================");
    }
}
