package com.j2core.elozovan.week05.io;

import com.j2core.elozovan.week05.data.DataPipeline;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class InputReaderThreadTest {

    @Test
    public void testInputReaderRun() throws Exception {
        DataPipeline.setQSizes(5, 5);
        Phaser phaser = new Phaser(1);

        new InputReaderThread("Test for Input processor",
                "/input.txt",
                DataPipeline.get(),
                phaser);

        while (!phaser.isTerminated()) { Thread.yield(); }

        assertEquals(DataPipeline.get().isResultQueueEmpty(), true);

        List<String> actualEntries = getActualEntries();
        List<String> expectedEntries = getExpectedEntries();
        assertEquals(actualEntries, expectedEntries);
    }

    private List<String> getExpectedEntries() {
        List<String> expectedEntries = new ArrayList<>();
        expectedEntries.add("Abzats 1.");
        expectedEntries.add("Ab, za ts 2.");
        expectedEntries.add("A, b, za t. s 3.");
        return expectedEntries;
    }

    private List<String> getActualEntries() {
        List<String> actualEntries = new ArrayList<>();
        String entry;

        while (null != (entry = DataPipeline.get().pollInputEntry(500, TimeUnit.MILLISECONDS))) {
            actualEntries.add(entry);
        }
        return actualEntries;
    }
}