package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.LinkDaoImpl;
import com.j2core.elozovan.week06.crawler.dao.ResultDaoImpl;
import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.networker.NetworkerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Implements the task.
 */
public class Crawler {
    private final static Logger LOGGER = LoggerFactory.getLogger(Crawler.class);
    //TODO: Should be configurable via .properties.
    private final static int NUMBER_OF_PROCESSOR_THREADS = 20;

    public static void main( String[] args ) {
        new Crawler().run();
    }

    private void run() {
        //General steps:
        // 1. Read/get next URL to process;
        // 2. Get HTML page by the URL;
        // 3. Process the page;
        // 4. Save processing results;
        // 5. Return to N1.

        Thread inpSupThread = new Thread(
                new InputSupplier(CrawlerDataPipeline.get(), new LinkDaoImpl()),
                "Input supplier.");
        inpSupThread.start();

        ExecutorService processorThreadsPool = Executors.newFixedThreadPool(NUMBER_OF_PROCESSOR_THREADS);
        for (int i=0; i < NUMBER_OF_PROCESSOR_THREADS; i++) {
            processorThreadsPool.execute(
                    new Thread(new TaskProcessor(CrawlerDataPipeline.get(),
                                                    new PageProcessorImpl(),
                                                    new NetworkerImpl()),
                                                "Task processor N" + i + "."));
        }

        Thread outputThread = new Thread(new ResultSaver(
                                                    CrawlerDataPipeline.get(), new LinkDaoImpl(), new ResultDaoImpl()),
                                                    "Result saver.");
        outputThread.start();

        Thread resetterThread = new Thread(new Resetter(
                                             new LinkDaoImpl()),
                                             "Resetter.");

        resetterThread.start();

        //-------------------
        try {
            //TODO: Pools.
            inpSupThread.join();
            processorThreadsPool.shutdown();
            outputThread.join();
            resetterThread.join();
        } catch (InterruptedException e) {
            LOGGER.error("Error upon waiting for the threads to finish.", e);
        }
    }
}