package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.LinkDao;
import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.utils.Props;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Supplies tasks (link records) into input queue.
 */
class InputSupplier implements Runnable {
    private final static Logger LOGGER = LoggerFactory.getLogger(InputSupplier.class);
    //TODO: Think about usage of pure DataPipeline.
    private CrawlerDataPipeline data;
    private InheritableThreadLocal<LinkDao> linkDaoThreadLocal = new InheritableThreadLocal<>();

    InputSupplier(CrawlerDataPipeline input, LinkDao linkDao) {
        data = input;
        linkDaoThreadLocal.set(linkDao);//TODO: Use Spring IoC/DI.
    }

    public void run() {
        LOGGER.info("{}. Starting new input supplier.", Thread.currentThread().getName());

        while (true) { //TODO: Need more sophisticated condition.
            Link candidate = linkDaoThreadLocal.get().selectOneForProcessing(Props.maxDepthLevel());

            //TODO: Need to handle external links.
            if (null != candidate
                    && null != candidate.getPageUrl()
                    && !candidate.getPageUrl().isEmpty()) {
                data.putInputEntry(candidate);
            } else {
                //No input data in the DB case. Wait for some time and try again.
                LOGGER.info("\n[???]\nWaiting for input data to appear in the DB.\n\n");
                try {
                    Thread.sleep(Props.waitForInput()); //Wait for input data.
                } catch (InterruptedException e) {}
            }
        }
    }
}