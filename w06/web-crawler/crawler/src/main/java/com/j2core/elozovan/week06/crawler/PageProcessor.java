package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * HTML page processor interface.
 */
public interface PageProcessor {
    PageProcessingResult process(String pageContent, String pageUrl, Link seedLink);
}