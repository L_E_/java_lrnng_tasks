package com.j2core.elozovan.week06.crawler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import com.j2core.elozovan.week06.networker.HtmlHelper;
import com.j2core.elozovan.week06.text_stats.StatsCalc;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * HTML page processor - gets links, text etc from a page.
 */
public class PageProcessorImpl implements PageProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(PageProcessorImpl.class);

    /**
     * Extracts text, links etc from the page provided.
     * @param pageContent HTML page to process.
     * @param seedLink - the starting point of current crawling. Need it to determine whether a link is external or not.
     * @return page processing result instance.
     */
    @Override
    public PageProcessingResult process(String pageContent, String pageUrl, Link seedLink) {
        HtmlHelper htmlHelper = new HtmlHelper();
        Document page = htmlHelper.parseHtmlContent(pageContent);
        String pageText = htmlHelper.getPageVisibleText(page);
        List<String> linksAsStrings = htmlHelper.getLinks(page);

        Set<Link> links = convertToLinks(seedLink, linksAsStrings); //Set to avoid duplicates at this stage.
        String json = getResult(pageText);

        PageProcessingResult result = new PageProcessingResult(pageUrl, pageText, json);
        result.setLinks(links);
        result.setSeedLink(seedLink);

        return result;
    }

    private String getResult(String pageText) {
        String json = "NO DATA, see logs.";
        if (null != pageText && !"".equals(pageText)) {
            Map<String, Integer> words = new StatsCalc().calculateWordsHistogram(pageText);

            ObjectMapper mapper = new ObjectMapper();

            try {
                json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(words);
            } catch (JsonProcessingException e) {
                LOGGER.error("Could not form text statistics in JSON format.", e);
            }
        }

        return json;
    }

    private Set<Link> convertToLinks(Link seedLink, List<String> linksAsStrings) {
        seedLink.getRootPageUrl();
        String rootPageUrl = seedLink.getRootPageUrl() == null || seedLink.getRootPageUrl().isEmpty() ? seedLink.getPageUrl() : seedLink.getRootPageUrl();
        Set<Link> links = new HashSet<>();
        for (String link : linksAsStrings) {
            boolean isExternal = true;

            if (link.startsWith(rootPageUrl)) { //Internal link.
                isExternal = false;
            }

            links.add(new Link(link,
                                rootPageUrl,
                                seedLink.getDepthLevel() + 1,
                                ProcessingState.NOT_PROCESSED,
                                isExternal));
        }

        return links;
    }
}