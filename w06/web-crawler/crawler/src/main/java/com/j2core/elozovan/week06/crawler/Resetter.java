package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.LinkDao;
import com.j2core.elozovan.week06.crawler.utils.Props;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Goes through URL records and resets statuses from IN_PROGRESS to NOT_PROCESSED for abandoned records.
 */
public class Resetter implements Runnable {
    private final static Logger LOGGER = LoggerFactory.getLogger(Resetter.class);
    private InheritableThreadLocal<LinkDao> linkDaoThreadLocal = new InheritableThreadLocal<>();

    public Resetter(LinkDao linkDao) {
        linkDaoThreadLocal.set(linkDao);//TODO: Use Spring IoC/DI.
    }

    public void run() {
        LOGGER.info("{}. Starting new Resetter.", Thread.currentThread().getName());

        while (true) { //TODO: Need more sophisticated condition.
            if (linkDaoThreadLocal.get().findAbandonedOneAndResetStatus(Props.abandonedTimeout())) {
             LOGGER.info("----------------\\Reset some record's status back to NOT_PROCESSED.");
            }

            //Just wait for some time, no need to reset the statuses too often.
            try {
                Thread.sleep(Props.waitBetweenResets());
            } catch (InterruptedException e) {}
        }
    }
}