package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.LinkDao;
import com.j2core.elozovan.week06.crawler.dao.ResultDao;
import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stores page processing results to the DB.
 */
public class ResultSaver implements Runnable {
    private final static Logger LOGGER = LoggerFactory.getLogger(ResultSaver.class);
    private CrawlerDataPipeline data;
    private InheritableThreadLocal<LinkDao> linkDaoThreadLocal = new InheritableThreadLocal<>();
    private InheritableThreadLocal<ResultDao> resultDaoThreadLocal = new InheritableThreadLocal<>();

    public ResultSaver(CrawlerDataPipeline data, LinkDao linkDao, ResultDao resultDao) {
        this.data = data;
        linkDaoThreadLocal.set(linkDao); //TODO: Spring IoC/DI.
        resultDaoThreadLocal.set(resultDao);
    }

    public void run() {
        LOGGER.info("{}. Starting new result saver.", Thread.currentThread().getName());

        while (true) { //TODO: Need more sophisticated condition.
            PageProcessingResult ppr = data.takeOutputEntry();

            // Step 4. Save processing results;
            linkDaoThreadLocal.get().storeLinksFound(ppr);

            //4.2. Store text.
            resultDaoThreadLocal.get().storePageTextProcessingResults(ppr);

            //4.3. Store input link as PROCESSED.
            linkDaoThreadLocal.get().updateLinkStatus(ppr.getSeedLink().getId(), ProcessingState.PROCESSED);
        }
    }
}