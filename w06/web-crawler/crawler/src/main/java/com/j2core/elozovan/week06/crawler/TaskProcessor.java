package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import com.j2core.elozovan.week06.networker.Networker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Processes tasks (get page content, process it etc).
 */
public class TaskProcessor implements Runnable {
    private final static Logger LOGGER = LoggerFactory.getLogger(TaskProcessor.class);
    private CrawlerDataPipeline data;
    private InheritableThreadLocal<Networker> netwrkrThreadLocal = new InheritableThreadLocal<>();
    private InheritableThreadLocal<PageProcessor> pageProcessorThreadLocal = new InheritableThreadLocal<>();

    public TaskProcessor(CrawlerDataPipeline dataPipeline, PageProcessor pageProcessor, Networker networker) {
        data = dataPipeline;
        pageProcessorThreadLocal.set(pageProcessor); //TODO: Use Spring for the injection.
        netwrkrThreadLocal.set(networker);
    }

    public void run() {
        LOGGER.info("{} starting new task processor.", Thread.currentThread().getName());

        while (true) { //TODO: Need more sophisticated condition.
            // Steps 2. Get the page;
            Link inputLink = data.takeInputEntry();

            String pageContent;
            try {
                pageContent = netwrkrThreadLocal.get().getPage(inputLink.getPageUrl());
            } catch (IOException e) {
                LOGGER.warn("Could not get page content using \n {} link due to \n", inputLink, e);
                return;
            }

            //Step 3. Process the page;
            PageProcessingResult ppr = pageProcessorThreadLocal.get().process(pageContent, inputLink.getPageUrl(), inputLink);
            data.putOutputEntry(ppr);
        }
    }
}