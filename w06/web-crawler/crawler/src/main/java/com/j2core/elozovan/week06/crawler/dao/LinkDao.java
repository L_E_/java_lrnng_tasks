package com.j2core.elozovan.week06.crawler.dao;

import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * "URLS" table access interface.
 */
public interface LinkDao {
    String URLS = "urls";

    String ID = "id";
    String URL = "url";
    String PARENT_URL = "parentUrl";
    String ROOT_URL = "rootUrl";
    String DEPTH_LEVEL = "depthLevel";
    String IS_EXTERNAL = "isExternal";
    String STATUS = "status";
    String URL_HASH = "urlHash";
    String TIME_MODIFIED = "timeModified";

    /**
     * Stores links found to DB.
     */
    boolean storeLinksFound(PageProcessingResult ppr);

    /**
     * Updates "status" column of DB record specified by ID.
     */
    boolean updateLinkStatus(int recordId, ProcessingState newStatus);

    /**
     * Returns a link object corresponding to a not-yet-processed DB record.
     * @param maxDepthLevel defines maximum allowed "depth" of crawling. I.e. the method shall not records having depthLevel > max.
     */
    Link selectOneForProcessing(int maxDepthLevel);

    /**
     * Finds an abandoned record and resets it status to NOT_PROCESSED.
     * Note, "abandoned" means "has status IN_PROGRESS fo a too long time".
     * @param abandonedTimeoutInSec
     */
    boolean findAbandonedOneAndResetStatus(int abandonedTimeoutInSec);
}