package com.j2core.elozovan.week06.crawler.dao;

import com.j2core.elozovan.week06.db_manipulator.DbManipulator;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Data access object for "URLS" table.
 * Contains table specific quarries.
 *
 * //TODO: Think about refactoring: remove duplication, provide method parameters etc.
 */
public class LinkDaoImpl implements LinkDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(LinkDaoImpl.class);

    /**
     * Stores all links(URLs) from the page processing result instance in DB.
     */
    @Override
    public boolean storeLinksFound(PageProcessingResult ppr) {
        //4.1. Store each external URL found.
        //TODO: Use BATCH!!!
        for (Link link : ppr.getLinks()) {
            if (link.isExternal()) { //TODO: Clarify, whether it shall be external ones only.
                List<String> columnNames = new ArrayList<>();
                columnNames.add(URL);
                columnNames.add(PARENT_URL);
                columnNames.add(ROOT_URL);
                columnNames.add(DEPTH_LEVEL);
                columnNames.add(IS_EXTERNAL);
                columnNames.add(URL_HASH);

                List<Object> values = new ArrayList<>();
                values.add(link.getPageUrl());
                values.add(ppr.getPageUrl());
                values.add(link.getRootPageUrl());
                values.add(link.getDepthLevel());
                values.add(link.isExternal());
                values.add(link.urlFingerprint()); // A way to avoid saving duplicate URLs. Just ignore "non_unique entry" exception.

                try {
                    DbManipulator.get().insertRecordIntoTable(URLS, columnNames, values);
                } catch (SQLException e) {
                    LOGGER.error("Could not save external links found.", e);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Updates DB URLS table record specified by id with new processing status value.
     *
     * @param recordId record-to-update's ID.
     * @param newStatus new processing satus value.
     * @return true is there were no issues upon the operation.
     */
    @Override
    public boolean updateLinkStatus(int recordId, ProcessingState newStatus) {
        //TODO: Multi-thread Singleton.
        Map<String, Object> columnNamesAndNewValues = new TreeMap<>();
        columnNamesAndNewValues.put(STATUS, newStatus.name());

        Map<String, Object> whereAndConditions = new TreeMap<>();
        whereAndConditions.put(ID, recordId);

        try {
            DbManipulator.get().updateRecordToTable(URLS, columnNamesAndNewValues, whereAndConditions);
        } catch (SQLException e) {
            LOGGER.error("Could not update link.", e);
            return false;
        }

        return true;
    }

    @Override
    public Link selectOneForProcessing(int maxDepthLevel) {
        String tableName = URLS;
        String slctWhereClause = STATUS + "='" + ProcessingState.NOT_PROCESSED.name()
                                + "' AND " + DEPTH_LEVEL + " <= " + maxDepthLevel;
        String columnToUpdateName = STATUS;
        Object newValue=ProcessingState.IN_PROGRESS.name();
        List<String> outputColumnNames = new ArrayList<>();
        outputColumnNames.add(ID);
        outputColumnNames.add(URL);
        outputColumnNames.add(PARENT_URL);
        outputColumnNames.add(ROOT_URL);
        outputColumnNames.add(DEPTH_LEVEL);
        outputColumnNames.add(IS_EXTERNAL);
        outputColumnNames.add(STATUS);

        Link result = new Link();

        try {
            Map<String, Object> rowData = DbManipulator.get().selectOneAndUpdate(
                    tableName,
                    slctWhereClause,
                    columnToUpdateName,
                    newValue,
                    outputColumnNames);

            if (null!=rowData && !rowData.isEmpty()) {
                result.setId((Integer)rowData.get(ID));
                result.setPageUrl((String)rowData.get(URL));
                result.setRootPageUrl((String)rowData.get(ROOT_URL));
                result.setDepthLevel((Integer)rowData.get(DEPTH_LEVEL));
                result.setIsExternal((Boolean)rowData.get(IS_EXTERNAL));
                result.setProcessingState((String)rowData.get(STATUS));
            }
        } catch (SQLException e) {
            LOGGER.error("Error upon getting an input URL.", e);
        }

        return result;
    }

    @Override
    public boolean findAbandonedOneAndResetStatus(int abandonedTimeoutInSec) {
        //SELECT * from urls WHERE status='IN_PROGRESS' AND timeModified <(SUBTIME(CURRENT_TIMESTAMP, SEC_TO_TIME('512')))

        String tableName = URLS;
        String slctWhereClause = STATUS + "='" + ProcessingState.IN_PROGRESS.name()
                + "' AND " + TIME_MODIFIED + " <= (SUBTIME(CURRENT_TIMESTAMP, SEC_TO_TIME('"+ abandonedTimeoutInSec +"')))";
        String columnToUpdateName = STATUS;
        Object newValue=ProcessingState.NOT_PROCESSED.name();

        List<String> outputColumnNames = new ArrayList<>();
        outputColumnNames.add(ID);

        try {
            DbManipulator.get().selectOneAndUpdate(
                    tableName,
                    slctWhereClause,
                    columnToUpdateName,
                    newValue,
                    outputColumnNames);

        } catch (SQLException e) {
            LOGGER.error("Error upon resetting statuses of an abandoned record.", e);
        }

        return true;
    }
}