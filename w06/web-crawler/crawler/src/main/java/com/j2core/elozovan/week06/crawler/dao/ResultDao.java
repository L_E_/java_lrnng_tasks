package com.j2core.elozovan.week06.crawler.dao;

import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * Data access interface for "RESULTS" table.
 */
public interface ResultDao {
    String URL = "url";
    String PAGE_TEXT = "pageText";
    String PAGE_STATS = "pageStats";
    String RESULTS = "results";

    boolean storePageTextProcessingResults (PageProcessingResult ppr);
}