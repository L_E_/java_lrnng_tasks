package com.j2core.elozovan.week06.crawler.dao;

import com.j2core.elozovan.week06.db_manipulator.DbManipulator;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for "RESULTS" table.
 * Contains table specific quarries.
 */
public class ResultDaoImpl implements ResultDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(ResultDaoImpl.class);

    /**
     * Stores page processing result object instance into DB.
     * @param ppr - the data to store.
     * @return true is there were no issues upon storing.
     */
    @Override
    public boolean storePageTextProcessingResults(PageProcessingResult ppr) {
        List<String> columnNames = new ArrayList<>();

        columnNames.add(URL);
        columnNames.add(PAGE_TEXT);
        columnNames.add(PAGE_STATS);

        List<Object> values = new ArrayList<>();
        values.add(ppr.getPageUrl());
        values.add(ppr.getPageText());
        values.add(ppr.getTextProcessingResult());

        try {
            DbManipulator.get().insertRecordIntoTable(RESULTS, columnNames, values);
        } catch (SQLException e) {
            LOGGER.error("Could not store page text processing results.", e);
            return false;
        }

        return true;
    }
}
