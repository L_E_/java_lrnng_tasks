package com.j2core.elozovan.week06.crawler.data;

import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * Crawler-specific data pipeline singleton.
 */
public class CrawlerDataPipeline extends DataPipeline<Link, PageProcessingResult> {
    private static volatile CrawlerDataPipeline instance;

    public static CrawlerDataPipeline get() {
        CrawlerDataPipeline localInstance = instance;
        if (localInstance == null) {
            synchronized (CrawlerDataPipeline.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new CrawlerDataPipeline();
                    setQSizes(2, 32); //TODO: Move the values to .properties.
                }
            }
        }

        return localInstance;
    }
}
