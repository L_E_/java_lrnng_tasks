package com.j2core.elozovan.week06.crawler.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Data pipeline.
 * Contains Input and Output blocking queues and allows
 * to put/get data into/from them.
 */
public class DataPipeline<I,O> {
    private final static Logger LOGGER = LoggerFactory.getLogger(DataPipeline.class);

    public static final int DEFAULT_INPUT_Q_SIZE = 128; //Some default value.
    public static final int DEFAULT_OUTPUT_Q_SIZE = 128; //Some default value.

    // These values are set only once, so they should be ThreadSafe.
    protected static boolean isQSizesSet = false;
    protected static int input_Q_size;
    protected static int output_Q_size;

    private AtomicInteger inputCount = new AtomicInteger(0);
    private AtomicInteger outputCount = new AtomicInteger(0);
    private volatile BlockingQueue<I> inputData;
    //TODO: Think about ConcurrentLinkedQueue<> usage (non-blocking one).
    private volatile BlockingQueue<O> outputData;

    protected DataPipeline() {
        if (!isQSizesSet) {
            input_Q_size = DEFAULT_INPUT_Q_SIZE;
            output_Q_size = DEFAULT_OUTPUT_Q_SIZE;
            isQSizesSet = true;
        }

        this.inputData = new LinkedBlockingQueue<>(input_Q_size);
        this.outputData = new LinkedBlockingQueue<>(output_Q_size);
    }

    public static void setQSizes(int inputQSize, int outputQSize) {
        input_Q_size = inputQSize;
        output_Q_size = outputQSize;
        isQSizesSet = true;
    }

    public boolean isInputQueueEmpty() {
        return inputData.isEmpty();
    }

    public I pollInputEntry(long timeout, TimeUnit unit) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("<-- Polling an input entry away.");
            }
            return inputData.poll(timeout, unit);
        } catch (InterruptedException e) {
            LOGGER.warn("Could not get input entry due to the interruption.", e);
        }

        return null;
    }

    public I takeInputEntry() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("<-- Taking an input entry away.");
        }
        try {
            return inputData.take();
        } catch (InterruptedException e) {
            LOGGER.error("Exception upon taking an input entry.", e);
        }

        //TODO: Think about something more graceful.
        return null;
    }

    public void putInputEntry(I newEntry) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("[\\/] Putting new input entry.");
            }
            inputData.put(newEntry);
            inputCount.getAndIncrement();
        } catch (InterruptedException e) {
            LOGGER.warn("Could not put input entry due to the interruption.", e);
        }
    }

    public boolean isOutputQueueEmpty() {
        return outputData.isEmpty();
    }

    public O pollOutputEntry(long timeout, TimeUnit unit) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("<--- Polling a result entry out of here.");
            }

            return outputData.poll(timeout, unit);
        } catch (InterruptedException e) {
            LOGGER.warn("Could not get result entry due to the interruption.", e);
        }

        return null;
    }

    public O takeOutputEntry() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("<-- Taking an output entry away.");
        }
        try {
            return outputData.take();
        } catch (InterruptedException e) {
            LOGGER.error("Exception upon taking an output entry.", e);
        }

        //TODO: Think about something more graceful.
        return null;
    }

    public void putOutputEntry(O newEntry) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("[\\/] Putting new output entry.");
            }
            outputData.put(newEntry);
            outputCount.getAndIncrement();
        } catch (InterruptedException e) {
            LOGGER.warn("Could not put output entry due to the interruption.", e);
        }
    }

    /**
     * Mainly for debug purposes.
     * Returns current number of entries in both queues (input - IQ and result(output) one - OQ).
     * Additionally shows total count of queued items i.e.
     * [IQ]=5/120 | [OQ]=20/40
     * means that IQ has currently 5 entries and 120 entries have been queued so far.
     */
    public String getStats() {
        return "[IQ]=" + inputData.size() + "/" + inputCount.intValue()
                + " | [OQ]=" + outputData.size() + "/" + outputCount.intValue();
    }
}