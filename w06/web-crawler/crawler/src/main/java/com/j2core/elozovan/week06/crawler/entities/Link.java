package com.j2core.elozovan.week06.crawler.entities;

import com.j2core.elozovan.week06.text_stats.StatsCalc;

/**
 * Input Link/URL DB record model.
 */
public class Link {
    private int id;
    private String pageUrl;
    private String rootPageUrl;
    private int depthLevel;
    private ProcessingState processingState;
    private boolean isExternal;

    public Link() { }

    public Link(String pageUrl, String rootPageUrl, int depthLevel, ProcessingState processingState, boolean isExternal) {
        this.pageUrl = pageUrl;
        this.rootPageUrl = rootPageUrl;
        this.depthLevel = depthLevel;
        this.processingState = processingState;
        this.isExternal = isExternal;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getPageUrl() {
        return pageUrl;
    }

    public String getRootPageUrl() {
        return rootPageUrl;
    }

    public int getDepthLevel() {
        return depthLevel;
    }

    public ProcessingState getProcessingState() {
        return processingState;
    }

    public void setProcessingState(ProcessingState processingState) {
        this.processingState = processingState;
    }

    public void setProcessingState(String processingStateName) {
        //TODO: validation + error handling.
        this.processingState = ProcessingState.valueOf(processingStateName);
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public void setRootPageUrl(String rootPageUrl) {
        this.rootPageUrl = rootPageUrl;
    }

    public void setDepthLevel(int depthLevel) {
        this.depthLevel = depthLevel;
    }

    public void setIsExternal(boolean isExternal) {
        this.isExternal = isExternal;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Link{");
        sb.append("id=").append(id);
        sb.append(", pageUrl='").append(pageUrl).append('\'');
        sb.append(", rootPageUrl='").append(rootPageUrl).append('\'');
        sb.append(", depthLevel=").append(depthLevel);
        sb.append(", processingState=").append(processingState);
        sb.append(", isExternal=").append(isExternal);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        return id == link.id && depthLevel == link.depthLevel && isExternal == link.isExternal && !(pageUrl != null ? !pageUrl.equals(link.pageUrl) : link.pageUrl != null) && !(rootPageUrl != null ? !rootPageUrl.equals(link.rootPageUrl) : link.rootPageUrl != null) && processingState == link.processingState;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (pageUrl != null ? pageUrl.hashCode() : 0);
        result = 31 * result + (rootPageUrl != null ? rootPageUrl.hashCode() : 0);
        result = 31 * result + depthLevel;
        result = 31 * result + (processingState != null ? processingState.hashCode() : 0);
        result = 31 * result + (isExternal ? 1 : 0);
        return result;
    }

    /**
     * Returns supposedly unique URL fingerprint.
     */
    public String urlFingerprint() {
        return new StatsCalc().calculateFingerprint(pageUrl);
    }
}