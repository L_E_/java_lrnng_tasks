package com.j2core.elozovan.week06.crawler.entities;

import java.util.Set;

/**
 * Page processing results container.
 */
public class PageProcessingResult {
    private Link seedLink;
    private String pageUrl;
    private String pageText;
    private String textProcessingResult;
    private Set<Link> links;

    public PageProcessingResult(String pageUrl, String pageText, String textProcessingResult) {
        this.pageUrl = pageUrl;
        this.pageText = pageText;
        this.textProcessingResult = textProcessingResult;
    }

    public Link getSeedLink() {
        return seedLink;
    }

    public void setSeedLink(Link seedLink) {
        this.seedLink = seedLink;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageText() {
        return pageText;
    }

    public void setPageText(String pageText) {
        this.pageText = pageText;
    }

    public String getTextProcessingResult() {
        return textProcessingResult;
    }

    public void setTextProcessingResult(String textProcessingResult) {
        this.textProcessingResult = textProcessingResult;
    }

    public Set<Link> getLinks() {
        return links;
    }

    public void setLinks(Set<Link> links) {
        this.links = links;
    }
}