package com.j2core.elozovan.week06.crawler.entities;

/**
 * Defines URL processing state
 */
public enum ProcessingState {
    NOT_PROCESSED, IN_PROGRESS, PROCESSED;
}
