package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class InputSupplierTest {
    private Thread inpSupThread;

    @Test
    public void testRun() throws Exception {
        inpSupThread = new Thread(
                new InputSupplier(CrawlerDataPipeline.get(), new TestLinkDaoImpl()),
                "Input supplier under test.");
        inpSupThread.start();

        inpSupThread.join(16);

        Assert.assertFalse(CrawlerDataPipeline.get().isInputQueueEmpty());
    }

    @AfterMethod
    void cleanUp() {
        if (null != inpSupThread && inpSupThread.isAlive()) {
            inpSupThread.interrupt();
        }
    }
}