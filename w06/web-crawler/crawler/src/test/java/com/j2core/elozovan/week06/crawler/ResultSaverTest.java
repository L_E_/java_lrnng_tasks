package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class ResultSaverTest {
    private Thread thread;

    @Test
    public void testRun() throws Exception {

        PageProcessingResult ppr = new PageProcessingResult("http://gde.to.tam", "....", "q:2");
        Link seed = new Link("http://tam.to", "", 1, ProcessingState.PROCESSED, true);
        seed.setId(100500);
        ppr.setSeedLink(seed);

        CrawlerDataPipeline.get().putOutputEntry(ppr);

        thread = new Thread(new ResultSaver(
                CrawlerDataPipeline.get(), new TestLinkDaoImpl(), new TestResultDaoImpl()),
                "Result saver under test.");

        thread.start();

        thread.join(64);

        Assert.assertTrue(CrawlerDataPipeline.get().isOutputQueueEmpty());
    }

    @AfterMethod
    void cleanUp() {
        if (null != thread && thread.isAlive()) {
            thread.interrupt();
        }
    }
}