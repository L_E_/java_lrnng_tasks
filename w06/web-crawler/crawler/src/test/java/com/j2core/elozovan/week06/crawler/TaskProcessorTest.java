package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.data.CrawlerDataPipeline;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TaskProcessorTest {
    private Thread thread;

    @Test
    public void testRun() throws Exception {
        CrawlerDataPipeline.get().putInputEntry(new Link("http://test.tr", "", 0, ProcessingState.IN_PROGRESS, true));
        thread =  new Thread(
                new TaskProcessor(CrawlerDataPipeline.get(),
                        new TestPageProcessorImpl(),
                        new TestNetworkerImpl()),
                        "Task processor for test.");
        thread.start();

        thread.join(64);

        Assert.assertFalse(CrawlerDataPipeline.get().isOutputQueueEmpty());
    }

    @AfterMethod
    void cleanUp() {
        if (null != thread && thread.isAlive()) {
            thread.interrupt();
        }
    }
}