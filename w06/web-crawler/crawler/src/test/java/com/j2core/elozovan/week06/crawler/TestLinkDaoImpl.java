package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.LinkDao;
import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;
import com.j2core.elozovan.week06.crawler.entities.ProcessingState;

/**
 * LinkDao implementation to be used in tests.
 */
public class TestLinkDaoImpl implements LinkDao {
    @Override
    public boolean storeLinksFound(PageProcessingResult ppr) {
        return true;
    }

    @Override
    public boolean updateLinkStatus(int recordId, ProcessingState newStatus) {
        return true;
    }

    @Override
    public Link selectOneForProcessing(int maxDepthLevel) {
        return new Link("http://ggg.de.to", "tam.to", 1, ProcessingState.IN_PROGRESS, true);
    }

    @Override
    public boolean findAbandonedOneAndResetStatus(int abandonedTimeoutInSec) {
        return true;
    }
}