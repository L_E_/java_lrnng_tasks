package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.networker.Networker;

/**
 * Test implementation.
 */
public class TestNetworkerImpl implements Networker {

    @Override
    public String getPage(String url) { return ""; }
}