package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.entities.Link;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * An implementation to be used in tests.
 */
public class TestPageProcessorImpl implements PageProcessor {
    @Override
    public PageProcessingResult process(String pageContent, String pageUrl, Link seedLink) {
        return new PageProcessingResult("http://test.tr", "test page text", "test:1, page:1, text:1");
    }
}