package com.j2core.elozovan.week06.crawler;

import com.j2core.elozovan.week06.crawler.dao.ResultDao;
import com.j2core.elozovan.week06.crawler.entities.PageProcessingResult;

/**
 * An implementation to be used in tests.
 */
public class TestResultDaoImpl implements ResultDao{

    @Override
    public boolean storePageTextProcessingResults(PageProcessingResult ppr) {
        return true;
    }
}