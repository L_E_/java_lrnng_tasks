package com.j2core.elozovan.week06.db_manipulator;

import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * An entry point for DB related manipulations.
 * Mostly taken from http://www.mkyong.com/jdbc/jdbc-preparestatement-example-insert-a-record/
 *
 */
public class DbManipulator {
    private final static Logger LOGGER = LoggerFactory.getLogger(DbManipulator.class);

    //TODO: Move to properties and/or add corresponding setters (to support unit testing with an embedded DB).
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/j2c_wc?useUnicode=true&characterEncoding=UTF8";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    private static volatile DbManipulator instance;

    private DbManipulator() {
    }

    //TODO: Think about multi-thread singleton.
    public static DbManipulator get() {
        DbManipulator localInstance = instance;
        if (localInstance == null) {
            synchronized (DbManipulator.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DbManipulator();
                }
            }
        }

        return localInstance;
    }

    //TODO: Think about adding transactions.
    //TODO: UTF-8!!!
    public synchronized boolean insertRecordIntoTable(String tableName, List<String> columnNames, List<Object> values) throws SQLException {
        //TODO: Add check-for-null, handle empty entries, escaping etc. And generally - use Hibernate.
        String columnNamesAsString = Joiner.on(",").skipNulls().join(columnNames);
        String valuesAsString = "";
        for(int i = 0; i < values.size() - 1; i++) {
            valuesAsString += "?,";
        }
        valuesAsString += "?"; //The very last parameter, does not need ",".

        //Composing string like INSERT INTO TABLE_NAME (COL1, COL2) VALUES (?,?)
        String insertTableSQL = "INSERT INTO " + tableName
                + "(" + columnNamesAsString + ") VALUES"
                + "(" + valuesAsString + ")";
        return executeUpdateStatement(tableName, insertTableSQL, values);
    }

    /**
     * Executes SQL UPDATE statement.
     *
     * @param tableName - table to update.
     * @param columnNamesAndNewValues - Map of COLUMN_NAME|NEW_VALUE entries. It is better to use TreeMap to preserve the order.
     * @param whereAndConditions - MAP of COLUMN_NAME|CURRENT_VALUE entries to be used in WHERE clause. NOTE: the entries are joined via AND operator. It is better to use TreeMap to preserve the order.
     * @return true if the statement has been executed successfully.
     * @throws SQLException
     */
    public synchronized boolean updateRecordToTable(String tableName, Map<String, Object> columnNamesAndNewValues, Map<String, Object> whereAndConditions) throws SQLException {
        //TODO: Add check-for-null, handle empty entries, escaping etc. And generally - use Hibernate.
        String setsAsString = "";
        for(String columnName : columnNamesAndNewValues.keySet()) {
            setsAsString += columnName + " = ?,";
        }
        setsAsString = setsAsString.substring(0, setsAsString.length() - 1); //The very last entry does not need ",".

        String wheresAsString = getWhereClause(whereAndConditions);

        //Composing string like UPDATE TABLE_NAME SET COL1 = ?, COL2 = ? WHERE FIELD1 = ? AND FIELD2 = ?
        String updateTableSQL = "UPDATE " + tableName
                + " SET "
                + setsAsString
                + " WHERE "
                + wheresAsString;

        List<Object> values = new ArrayList<>();

        for (Object obj : columnNamesAndNewValues.values()) {
            values.add(obj);
        }
        for (Object obj : whereAndConditions.values()) {
            values.add(obj);
        }

        return executeUpdateStatement(tableName, updateTableSQL, values);
    }

    public synchronized List<Map<String, Object>> selectAllRecordsFromTableMatchingAllConditions(String tableName, List<String> columnNames, Map<String, Object> whereAndConditions) throws SQLException {
        //TODO: REUSE DB Connection ?
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        //Composing string like SELECT * FROM TABLE_NAME WHERE FIELD1 = ? AND FIELD2 = ?
        String selectSQL = "SELECT * FROM "
                            + tableName
                            + " WHERE "
                            + getWhereClause(whereAndConditions);

        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);

            List<Object> values = new ArrayList<>();

            for (Object obj : whereAndConditions.values()) {
                values.add(obj);
            }
            for (int i=0; i < values.size(); i++) {
                preparedStatement.setObject(i + 1, values.get(i));
            }

            ResultSet rs = preparedStatement.executeQuery();
            List<Map<String, Object>> result = new ArrayList<>();

            while (rs.next()) {
                Map<String, Object> row = new TreeMap<>();

                for (String columnName : columnNames) {
                    row.put(columnName, rs.getObject(columnName));
                }

                result.add(row);
            }
            return result;
        } catch (SQLException e) {
            LOGGER.warn("SELECT ALL query for {} failed.", tableName, e);
            return null;
        } finally {
            if (preparedStatement != null) { preparedStatement.close(); }
            if (dbConnection != null) { dbConnection.close(); }
        }
    }

    public Map<String, Object> selectOneAndUpdate(String tableName,
                                                    String slctWhereClause,
                                                    String columnToUpdateName,
                                                    Object newValue,
                                                    List<String> outputColumnNames) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;

        String selectTableSQL = "SELECT * from "
                                + tableName
                                + " WHERE "
                                + slctWhereClause
                                + " LIMIT 1 FOR UPDATE";

        try {
            dbConnection = getDBConnection();
            dbConnection.setAutoCommit(false);
            statement = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            LOGGER.info("Select 1 for update, slct query: {}", selectTableSQL);

            // execute select SQL statement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            Map<String, Object> resultRow = new TreeMap<>();
            while (rs.next()) {
                int targetIndex = rs.findColumn(columnToUpdateName);
                rs.updateObject(targetIndex, newValue);
                rs.updateRow();

                for (String columnName : outputColumnNames) {
                    resultRow.put(columnName, rs.getObject(columnName));
                }

            }

            dbConnection.commit();
            dbConnection.setAutoCommit(true);

            return resultRow;
        } catch (SQLException e) {
            LOGGER.error("Could not select for update.", e);
            return null;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    private String getWhereClause(Map<String, Object> whereAndConditions) {
        String wheresAsString = "";
        String additionalString = "";
        if (whereAndConditions.keySet().size() > 1) {
            additionalString = " AND ";
        }

        for(String columnName : whereAndConditions.keySet()) {
            wheresAsString += columnName + " = ? " + additionalString;
        }
        return wheresAsString;
    }

    private boolean executeUpdateStatement(String tableName, String sqlStatement, List<Object> values) throws SQLException {
        //TODO: REUSE DB Connection ?
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(sqlStatement);

            for (int i=0; i < values.size(); i++) {
                preparedStatement.setObject(i + 1, values.get(i));
            }

            preparedStatement.executeUpdate();

            LOGGER.info("SQL statement has been executed for {} table. Values: {}", tableName, Joiner.on(",").useForNull("NULL").join(values));
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Could not execute statement for {} table.", tableName, e);
            return false;
        } finally {
            if (preparedStatement != null) { preparedStatement.close(); }

            if (dbConnection != null) { dbConnection.close(); }
        }
    }

    private Connection getDBConnection() {
        Connection dbConnection = null;

        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Cannot get JDBC driver.", e);
        }

        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            LOGGER.warn("Cannot establish DB connection.", e);
        }

        return dbConnection;
    }
}