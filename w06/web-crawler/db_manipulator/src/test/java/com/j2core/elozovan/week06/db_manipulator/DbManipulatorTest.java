package com.j2core.elozovan.week06.db_manipulator;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.*;

/**
 * //TODO: Re-work to use an embedded DB.
 *
 * At the moment this is just a set of semi-manual tests.
 */
public class DbManipulatorTest {

    @Test (enabled = false)
    //@Test
    public void testInsertRecordIntoTable() throws Exception {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("url");
        columnNames.add("entry");

        List<Object> values = new ArrayList<>();
        values.add("http://ya.ru");
        values.add("{words:10, dots:20}");

        Assert.assertTrue(DbManipulator.get().insertRecordIntoTable("results", columnNames, values));
    }

    @Test (enabled = false)
    //@Test
    public void testUpdateRecordToTable() throws Exception {
        Map<String, Object> columnNamesAndNewValues = new TreeMap<>();
        columnNamesAndNewValues.put("url", "yandex.ru");
        columnNamesAndNewValues.put("entry", "{{test}}");

        Map<String, Object> whereAndConditions = new TreeMap<>();
        whereAndConditions.put("url", "http://ya.ru");

        Assert.assertTrue(DbManipulator.get().updateRecordToTable("results", columnNamesAndNewValues, whereAndConditions));
    }

    @Test (enabled = false)
    //@Test
    public void testSelectAll() throws Exception {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("id");
        columnNames.add("url");
        columnNames.add("entry");

        Map<String, Object> whereAndConditions = new TreeMap<>();
        whereAndConditions.put("url", "yandex.ru");

        Assert.assertTrue(null != DbManipulator.get().selectAllRecordsFromTableMatchingAllConditions("results", columnNames, whereAndConditions));
    }

    @Test (enabled = false)
    //@Test
    public void testSelectOneAndUpdate() throws Exception {
        String tableName = "urls";
        String slctWhereClause = "status='NOT_PROCESSED'";
        String columnToUpdateName = "status";
        Object newValue="IN_PROGRESS";
        List<String> outputColumnNames = new ArrayList<>();
        outputColumnNames.add("id");
        outputColumnNames.add("url");
        outputColumnNames.add("parentUrl");
        outputColumnNames.add("rootUrl");
        outputColumnNames.add("depthLevel");
        outputColumnNames.add("isExternal");
        outputColumnNames.add("status");

        Assert.assertTrue(null != DbManipulator.get().selectOneAndUpdate(
                                            tableName,
                                            slctWhereClause,
                                            columnToUpdateName,
                                            newValue,
                                            outputColumnNames));
    }
}