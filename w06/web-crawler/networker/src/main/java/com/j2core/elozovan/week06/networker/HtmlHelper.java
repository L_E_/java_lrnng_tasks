package com.j2core.elozovan.week06.networker;

import com.google.common.base.Preconditions;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses HTML documents.
 * Based on http://jsoup.org/cookbook/extracting-data/attributes-text-html .
 */
public class HtmlHelper {

    /**
     * Gets visible text of the page.
     *@param  page - the page as a Jsoup document.
     */
    public String getPageVisibleText(Document page) {
        Preconditions.checkNotNull(page, "Page instance shall not be null.");

        if (null != page.body()) {
            return page.body().text();
        }

        return "";
    }

    /**
     * Gets visible text of the page.
     * @param pageContent - page's HTML content as a String.
     *
     */
    public String getPageVisibleText(String pageContent) {
        Preconditions.checkNotNull(pageContent, "Page content shall not be null.");

        return getPageVisibleText(parseHtmlContent(pageContent));
    }

    /**
     * Returns list of external and internal URLs except link pointing to the page itself.
     */
    public List<String> getLinks(Document page) {
        Preconditions.checkNotNull(page, "Page instance shall not be null.");

        List<String> urls = new ArrayList<String>();
        Elements links = page.select("a[href]");

        for (Element link : links) {
            String tmp = link.attr("abs:href"); //Internal links wil be resolved using the page's URL.
            if (!tmp.equals(page.location())) { //Avoid links to the page itself.
                urls.add(tmp);
            }
        }

        return urls;
    }

    /**
     * Returns list of external and internal URLs except link pointing to the page itself.
     */
    public List<String> getLinks(String pageContent) {
        Preconditions.checkNotNull(pageContent, "Page content shall not be null.");

        return getLinks(parseHtmlContent(pageContent));
    }

    /**
     * Converts string pageContent to Jsoup's Document if possible.
     */
    public Document parseHtmlContent(String pageContent){
        return Jsoup.parse(pageContent);
    }
}