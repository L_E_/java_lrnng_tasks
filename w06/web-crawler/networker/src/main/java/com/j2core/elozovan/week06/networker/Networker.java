package com.j2core.elozovan.week06.networker;

import java.io.IOException;

/**
 * Network access helper interface.
 */
public interface Networker {
    String getPage(String url) throws IOException;
}