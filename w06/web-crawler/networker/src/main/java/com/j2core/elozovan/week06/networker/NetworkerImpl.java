package com.j2core.elozovan.week06.networker;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Network access helper.
 * Primary role - get a web-page content.
 */
public class NetworkerImpl implements Networker {
    private final static Logger LOGGER = LoggerFactory.getLogger(NetworkerImpl.class);

    private final static String USER_AGENT = "Mozilla/5.0";

    @Override
    public String getPage(String url) throws IOException {
        //Code snippets are boldly taken from http://www.mkyong.com/java/apache-httpclient-examples/
        // and http://stackoverflow.com/questions/19517538/ignoring-ssl-certificate-in-apache-httpclient-4-3
        // and http://hc.apache.org/httpcomponents-client-4.3.x/httpclient/examples/org/apache/http/examples/client/ClientCustomSSL.java
        // and http://stackoverflow.com/questions/6908948/java-sun-security-provider-certpath-suncertpathbuilderexception-unable-to-find
        return getPageContentAsString(url);
    }

    private String getPageContentAsString(String url) throws IOException {
        HttpGet request = new HttpGet(url);

        request.setHeader("User-Agent", USER_AGENT);
        CloseableHttpResponse response = null;
        String result = "";

        SSLContextBuilder builder = new SSLContextBuilder();

        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext sslcontext = SSLContexts.custom()
                    .useProtocol("TLSv1")
                    .loadTrustMaterial(trustStore, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                            return true;
                        }
                    })
                    .build();


            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext,
                    NoopHostnameVerifier.INSTANCE);
            CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

            LOGGER.info("Sending 'HTTP GET' to {}", url);
            response = httpclient.execute(request);
            LOGGER.info("Response code : " + response.getStatusLine().getStatusCode());

            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity, "UTF-8");
            EntityUtils.consumeQuietly(entity);
        } catch (IOException | KeyStoreException | KeyManagementException | NoSuchAlgorithmException e) {
            LOGGER.warn("Could not get/read page content using {} URL.", url, e);
        } finally {
            if (null != response) {
                response.close();
            }
        }

        return result;
    }
}