package com.j2core.elozovan.week06.networker;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;


public abstract class AbstractNetworkerTest {
    private final static int PORT = 23210;
    protected final static String TEST_URL = "http://localhost:" + PORT;

    private WebApp testWebApp;

    @BeforeClass
    public void setUp() throws IOException {
        testWebApp = new WebApp(PORT);
        testWebApp.run();
    }

    @AfterClass
    public void tearDown() {
        testWebApp.stop();
    }
}