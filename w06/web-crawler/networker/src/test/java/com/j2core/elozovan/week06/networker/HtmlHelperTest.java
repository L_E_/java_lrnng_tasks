package com.j2core.elozovan.week06.networker;

import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;
/**
 */
public class HtmlHelperTest {
    private static final String PAGE_CONTENT = "<html><body><h1>Test web app.</h1></body></html>";
    private static final String PAGE_CONTENT_W_LINK = "<html><body><a href='http://ya.ru'>Ya.ru</a>";

    @Test
    public void testGetVisiblePageText(){
        HtmlHelper helper = new HtmlHelper();
        String actual = helper.getPageVisibleText(PAGE_CONTENT);
        assertEquals(actual, "Test web app.");

        actual = helper.getPageVisibleText(helper.parseHtmlContent(PAGE_CONTENT));
        assertEquals(actual, "Test web app.", "Something wrong with parsing to Document and getting visible text out of it.");
    }

    @Test
    public void testGetLinks(){
        HtmlHelper helper = new HtmlHelper();
        List<String> actual = helper.getLinks(PAGE_CONTENT_W_LINK);
        assertNotNull(actual);
        assertEquals(actual.size(), 1);
        assertEquals(actual.get(0), "http://ya.ru");

        actual = helper.getLinks(helper.parseHtmlContent(PAGE_CONTENT_W_LINK));
        String msg = "Something wrong with parsing to Document and getting links out of it.";
        assertNotNull(actual, msg);
        assertEquals(actual.size(), 1, msg);
        assertEquals(actual.get(0), "http://ya.ru", msg);
    }
}