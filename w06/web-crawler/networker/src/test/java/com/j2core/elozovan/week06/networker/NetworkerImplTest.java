package com.j2core.elozovan.week06.networker;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NetworkerImplTest extends AbstractNetworkerTest {

    @Test
    public void testGetPage() throws Exception {
        String actual = new NetworkerImpl().getPage(TEST_URL);
        String expected = "<html><body><h1>Test web app.</h1></body></html>";

        assertEquals(actual, expected);
    }
}