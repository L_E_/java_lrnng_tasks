package com.j2core.elozovan.week06.networker;

import fi.iki.elonen.NanoHTTPD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class WebApp extends NanoHTTPD {
    private final static Logger LOGGER = LoggerFactory.getLogger(WebApp.class);

    public WebApp(int port) throws IOException { super(port); }

    public void run() {
        try {
            start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
            LOGGER.info("Started a web app for tests.");
        } catch (IOException ioe) {
            LOGGER.error("Could not start the web app for tests.");
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        String msg = "<html><body><h1>Test web app.</h1>";
        return newFixedLengthResponse(msg + "</body></html>");
    }
}