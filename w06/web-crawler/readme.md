Code Task
Main idea of this task is to create high-performance web-crawler, backed by database.
Crawler itself is a simple tool which
    starts from some starting URL, downloads page
    parse links (local and external)
    parses page content (filtering out non-displaying content like scripts or hidden parameters or html tags)
    saves page content processed to database
    saves links as a new starting points for later processing.
Crawler must
    save it's state to database.
    be multiple-threaded instance.
    be able to be started in a few application instances.
    not visit the same page twice.
    support max. depth limits, for example 20 external hops from the starting point.
    be able to support HTTPS
If crawler finished all available tasks, it should wait for a new tasks.
If crawler died or was stopped, another crawler instance must continue unfinished work.
Downloaded page must be parsed to separate words, so for every parsed page we need to save list of words and how many times they was used.

==========================================================================================================
//General steps:
// 1. Read/get next URL to process (wait to be available and filter duplicates and processed ones);
// 2. Get HTML page by the URL;
// 3. Process the page (get text, get words, get words stats etc);
// 4. Save processing results (the text, the words stats  + change input's state to PROCESSED);
// 5. Return to N1.

Modules:
DB_Manipulator - JBDC based DB facade. At the moment - just a rough helper to work with MySQL DB. Contains hard-coded entries, number of todos etc;
Networker - provides getting and parsing page content features;
Text_Statistician - text processing module (words, words count etc);

Crawler - the crawler itself.
==========================================================================================================
Details:

Stage 1, Read/get ... - InputSupplier reads input URL record from the DB using corresponding DAO (e.g. LinkDao).
The URL is further saved in the input blocking queue instance (as a Link object instance, see CrawlerDataPipeline).
Note 1, the "reading" is in fact a "select for update + update" transaction i.e. something like
 SELECT * FROM URLS WHERE STATUS='NOT_PROCESSED' LIMIT 1 FOR UPDATE
 UPDATE URLS SET STATUS='IN_PROGRESS' WHERE ... .
Note 2, pure duplicates filtering is not implemented yet. However, the "status" allows not to read
those URLs which were precessed already or in process right now. Also, LinkDao does not save records having
not unique urlHashCode.

Stage2, Get HTML ... - a Link instance is polled out of the input queue. HTML document addressed by the link is downloaded
with help of Networker.

Stage 3, Process the page ... - the page is parsed into PageProcessingResult instance by PageProcessor with help of HtmlHelper.

Stage 4, Save processing results ... - See ResultSaver class. Saving is done by invoking corresponding methods of LinkDao and ResultDao.
URLs are saved into "urls" table by LinkDao. Note, each record contains an URL itself, its parent URL (i.e. the immediate parent),
the root URL - the very first starting point of the Crawler. depthLevel is the distance in hops between "url" and "rootUrl".

Page processing results (words, words count etc) are stored into "results" table via ResultDao.


================
Note on "If crawler died or was stopped, another crawler instance must continue unfinished work." -

Need to add "timestamp" column to "urls" table. Add a separate "watcher" thread which should
check "IN_PROGRESS" url records and if timestamp <<< current timestamp consider that as "a crawler instance
has not completed processing", so such records should be switched back to "NOT_PROCESSED".

================

DB structure.
TABLE  urls ( // Contains both processed and not processed yet URLs.
  id INT(11) NOT NULL AUTO_INCREMENT,
  url VARCHAR(4096) NOT NULL, // The page-of-interest's URL
  parentUrl VARCHAR(4096),    // Immediate parent of the page.
  rootUrl VARCHAR(4096),      // The very first page in the branch, a starting point of the crawling, depth level = 0
  depthLevel INT(11) DEFAULT 0, // Defines distance in "hops" from a root page.
  isExternal BOOL, // Defines whether an URL is "external" i.e. points to some other site than parent/root to.
  status VARCHAR(32) DEFAULT "NOT_PROCESSED", // Processing state. See ProcessingState enum.
  PRIMARY KEY (id)
  urlHash varchar(100) NOT NULL; // An unique fingerprint.
  timeModified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP; // Last time the record was modified (to handle records abandoned in the middle of processing)
)

TABLE results ( // Contains input record processing results.
  id INT(11) NOT NULL AUTO_INCREMENT,
  url VARCHAR(4096) NOT NULL,
  pageText TEXT DEFAULT NULL,
  pageStats TEXT DEFAULT NULL,
  PRIMARY KEY (id)
)
