package com.j2core.elozovan.week06.text_stats;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.hash.Hashing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A helper class which calculates text statistics.
 */
public class StatsCalc {

    public int calculateDotsCount(String entry){
        return getLengthDiffAfterRemovingSubstring(entry, "\\.");
    }

    public int calculateCommasCount(String entry){
        return getLengthDiffAfterRemovingSubstring(entry, ",");
    }

    public String calculateFingerprint(String entry) {
        return Hashing.sha1().hashString(entry, Charsets.UTF_8).toString();
    }

    public int calculateWordsCount(String entry) {
        return getListOfWords(entry).size();
    }

    public int calculateAverageWordLength(String entry) {
        List<String> words = getListOfWords(entry);
        long sum = 0;

        for (String word : words) {
            sum += word.length();
        }

        int size = words.size();
        return size != 0 ? (int) (sum / words.size()) : 0;
    }

    /**
     * "....how many times they was used...
     *
     * @return Map<String, Integer> => Map<word, count of entries>
     */
    public Map<String, Integer> calculateWordsHistogram(List<String> words) {
//TODO: Use Apache Commons' commons-collections4, a Bag.
        Map<String, Integer> histogram = new HashMap<String, Integer>();

        for (String word : words) {
            if (histogram.containsKey(word)) {
                histogram.put(word, histogram.get(word) + 1);
            } else {
                histogram.put(word, 1);
            }
        }

        return histogram;
    }

    /**
     * "....how many times they was used...
     *
     * @return Map<String, Integer> => Map<word, count of entries>
     */
    public Map<String, Integer> calculateWordsHistogram(String text) {
        return calculateWordsHistogram(getListOfWords(text));
    }

    public List<String> getListOfWords(String entry) {
        return Splitter
                .onPattern("([,.]|\\s)")
                .trimResults()
                .omitEmptyStrings()
                .splitToList(entry);
    }

    private int getLengthDiffAfterRemovingSubstring(String entry, String subStringToRemove) {
        int before = entry.length();

        String tmp = entry.replaceAll(subStringToRemove, "");

        return before - tmp.length();
    }
}