package com.j2core.elozovan.week06.text_stats;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class StatsCalcTest {
    public static final String ENTRY = "321, 123, 456, 789.";

    @Test
    public void testCalculateDotsCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateDotsCount(ENTRY), 1);
        assertEquals(calc.calculateDotsCount(""), 0);
        assertEquals(calc.calculateDotsCount("... ."), 4);
    }

    @Test
    public void testCalculateCommasCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateCommasCount(ENTRY), 3);
        assertEquals(calc.calculateCommasCount(""), 0);
        assertEquals(calc.calculateCommasCount(",,, ."), 3);
    }

    @Test
    public void testCalculateFingerprint() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateFingerprint(ENTRY), "903e371645e741a8146745a245fc1eb24eedfc45");
        assertEquals(calc.calculateFingerprint(""), "da39a3ee5e6b4b0d3255bfef95601890afd80709");
    }

    @Test
    public void testCalculateWordsCount() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateWordsCount(ENTRY), 4);
        assertEquals(calc.calculateWordsCount(""), 0);
    }

    @Test
    public void testCalculateAverageWordLength() throws Exception {
        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateAverageWordLength(ENTRY), 3);
        assertEquals(calc.calculateAverageWordLength(""), 0);
    }

    @Test
    public void testGetListOfWords() throws Exception {
        StatsCalc calc = new StatsCalc();

        List<String> expected = new ArrayList<String>();
        expected.add("321");
        expected.add("123");
        expected.add("456");
        expected.add("789");
        assertEquals(calc.getListOfWords(ENTRY), expected);
    }

    @Test
    public void testCalculateWordsHistogram(){
        Map<String, Integer> expected = new HashMap<String, Integer>();
        expected.put("321", 1);
        expected.put("123", 1);
        expected.put("456", 1);
        expected.put("789", 1);

        StatsCalc calc = new StatsCalc();
        assertEquals(calc.calculateWordsHistogram(calc.getListOfWords(ENTRY)), expected);

        final String phrase = "Kra Zel Kra, Zel, Zel.";
        expected.clear();

        expected.put("Kra", 2);
        expected.put("Zel", 3);
        assertEquals(calc.calculateWordsHistogram(calc.getListOfWords(phrase)), expected);
    }
}