Code Task

You are creating simple bank accounts service, backed by the database.
 In the database you have to create 10 accounts with names account1, account2 etc. and $100,000 balance.
You have to start 10 threads for every account (total 100),
which will make 1000 transactions,
decreasing the balance onto $10 on every transaction in the database.
Limits: While synchronization you shouldn't block accounts,
which are not involved on the operation, so, for example,
account2 should NOT be blocked, when account1 updated.
Please consider threads like separate clients, who can't share anything.
So, no synchronization objects can be used.

==========================================================================================================
General ideas:
1. Use Hibernate ORM for optimistic DB locking.
If there is a stale record - try to commit the transaction several more times.
If still no luck - throw a RuntimeException. See AccountDaoImpl

2. Use pure JDBC in a quick-n-dirty manner to implement pessimistic DB locking approach
(SELECT ... FOR UPDATE). See AccountDaoPessimisticImpl.