package com.j2core.elozovan.week07.bank_account_updater;

/**
 * "Accounts" table access interface.
 */
public interface AccountDao {
    boolean selectByNameAndUpdateBalanceByDelta(String name, Double balanceDelta);
}