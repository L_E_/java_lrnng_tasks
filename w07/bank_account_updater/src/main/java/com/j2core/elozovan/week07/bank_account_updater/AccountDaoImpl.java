package com.j2core.elozovan.week07.bank_account_updater;

import com.j2core.elozovan.week07.bank_account_updater.entities.Account;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Hibernated based data access object for "Accounts" table.
 * Follows so called "optimistic DB locking approach".
 */
public class AccountDaoImpl implements AccountDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountDaoImpl.class);
    private final static int FAILURES_LIMIT = 50;

    @Override
    public boolean selectByNameAndUpdateBalanceByDelta(String name, Double balanceDelta) {
        boolean tryAgain;
        int failuresCount = 0;
        do {
            tryAgain = !tryToUpdate(name, balanceDelta);
            failuresCount++;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(" {} failN{} (account={})", Thread.currentThread().getName(), failuresCount, name);
            }
        } while (tryAgain && failuresCount < FAILURES_LIMIT);

        return !tryAgain;
    }

    private boolean tryToUpdate(String name, Double balanceDelta) {
        Session session = DbSessionProvider.get().getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List result = session.createQuery( "from Account where name='" + name + "'" ).list();
            Account acc = (Account) result.get(0);
            acc.setBalance(acc.getBalance() + balanceDelta);
            session.save(acc);
            transaction.commit();

            return true;
        } catch (RuntimeException e) {
            if (null != transaction) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(" {} [!!!] ROLLING BACK (account={})", Thread.currentThread().getName(), name);
                }
                transaction.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }

        return false;
    }
}