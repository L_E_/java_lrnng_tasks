package com.j2core.elozovan.week07.bank_account_updater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * An extremely  quick-n-dirty pessimistic locking based DAO.
 * A very straightforward approach, just for educational purposes.
 *
 */
public class AccountDaoPessimisticImpl implements AccountDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountDaoPessimisticImpl.class);

    //TODO: Move to properties and/or add corresponding setters (to support unit testing with an embedded DB).
    // Or - Spring JDBC || Hibernate
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/j2c_w07_ba?useUnicode=true&characterEncoding=UTF8";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    public boolean selectByNameAndUpdateBalanceByDelta(String name, Double balanceDelta) {
        Connection dbConnection = null;
        Statement statement = null;

        String selectAndLockSQL = "SELECT * from accounts "
                + " WHERE name='"
                + name
                + "' FOR UPDATE;";

        try {
            dbConnection = getDBConnection();
            dbConnection.setAutoCommit(false);
            statement = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            LOGGER.info("Select & lock, slct query: {}", selectAndLockSQL);

            // execute select SQL statement
            ResultSet rs = statement.executeQuery(selectAndLockSQL);

            while (rs.next()) {
                int targetIndex = rs.findColumn("balance");
                double currentBalance = rs.getDouble("balance");
                rs.updateObject(targetIndex, currentBalance + balanceDelta);
                rs.updateRow();
            }

            dbConnection.commit();
            dbConnection.setAutoCommit(true);

            return true;
        } catch (SQLException e) {
            LOGGER.error("Could not select for update.", e);
            return false;
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Oj, beda, beda, ogor4enie.", e);
            }
        }
    }

    private Connection getDBConnection() {
        Connection dbConnection = null;

        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Cannot get JDBC driver.", e);
        }

        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            LOGGER.warn("Cannot establish DB connection.", e);
        }

        return dbConnection;
    }
}