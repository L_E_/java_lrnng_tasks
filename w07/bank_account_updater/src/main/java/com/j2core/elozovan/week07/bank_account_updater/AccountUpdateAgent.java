package com.j2core.elozovan.week07.bank_account_updater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Updates specific account record.
 */
class AccountUpdateAgent implements Runnable {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountUpdateAgent.class);
    private InheritableThreadLocal<AccountDao> daoThreadLocal = new InheritableThreadLocal<>();
    private InheritableThreadLocal<Integer> transactionLimit = new InheritableThreadLocal<>();
    private InheritableThreadLocal<String> accountName = new InheritableThreadLocal<>();
    private InheritableThreadLocal<Double> balanceDelta = new InheritableThreadLocal<>();

    AccountUpdateAgent(String account, Double delta, AccountDao accountDao, Integer limit) {
        daoThreadLocal.set(accountDao);//TODO: Use Spring DI to inject.
        transactionLimit.set(limit);
        accountName.set(account);
        balanceDelta.set(delta);
    }

    public void run() {
        LOGGER.info("{}. Starting new account update agent. Acc. name is {}",
                    Thread.currentThread().getName(),
                    accountName.get());

        for (int i = 0; i < transactionLimit.get(); i++) {
            boolean result = act();
            if (!result) {
                LOGGER.warn("\n {} \n Trying for the 2-nd time {}/{}.", Thread.currentThread().getName(), accountName.get(), i);
                result = act(); //Try it again.
            }
            LOGGER.info("{} Updated {}({}). Iteration = {}",
                    Thread.currentThread().getName(),
                    accountName.get(),
                    result,
                    i);

            if (!result) {
                throw new RuntimeException("Could not update balance for " + accountName.get());
            }
        }
    }

    private boolean act() {
        return daoThreadLocal.get()
                .selectByNameAndUpdateBalanceByDelta(accountName.get(), balanceDelta.get());
    }
}