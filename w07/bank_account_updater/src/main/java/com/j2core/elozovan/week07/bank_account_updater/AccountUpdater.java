package com.j2core.elozovan.week07.bank_account_updater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Updates account records per the task.
 *
 */
public class AccountUpdater {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountUpdater.class);
    //TODO: Should be configurable via .properties.
    private final static int NUMBER_OF_THREADS_PER_ACCOUNT = 10;
    private final static double DELTA = -10.0;
    private final static int ITERATIONS = 1000;
    private final static List<String> ACCOUNTS = new ArrayList<>();

    static {
        ACCOUNTS.add("account1");
        ACCOUNTS.add("account2");
        ACCOUNTS.add("account3");
        ACCOUNTS.add("account4");
        ACCOUNTS.add("account5");
        ACCOUNTS.add("account6");
        ACCOUNTS.add("account7");
        ACCOUNTS.add("account8");
        ACCOUNTS.add("account9");
        ACCOUNTS.add("account10");
    }

    public static void main( String[] args ) {
        new AccountUpdater().run();
    }

    private void run() {
        long start = System.currentTimeMillis();
        ExecutorService processorThreadsPool = Executors.newFixedThreadPool(NUMBER_OF_THREADS_PER_ACCOUNT * ACCOUNTS.size());
        for (String accName : ACCOUNTS) {
            for (int i = 0; i < NUMBER_OF_THREADS_PER_ACCOUNT; i++){
                //Runnable agent = new AccountUpdateAgent(accName, DELTA, new AccountDaoImpl(), ITERATIONS);
                Runnable agent = new AccountUpdateAgent(accName, DELTA, new AccountDaoPessimisticImpl(), ITERATIONS);
                processorThreadsPool.execute(agent);
            }
        }

        try {
            processorThreadsPool.shutdown();
            processorThreadsPool.awaitTermination(16, TimeUnit.SECONDS);
        } catch (InterruptedException | SecurityException e) {
            LOGGER.error("Error upon waiting for the threads to finish.", e);
        }

        LOGGER.info("Finish. It took {}ms.", (System.currentTimeMillis()-start ) / 1000);
    }
}
