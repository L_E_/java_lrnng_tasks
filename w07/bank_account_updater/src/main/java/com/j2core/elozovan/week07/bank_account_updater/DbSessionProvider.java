package com.j2core.elozovan.week07.bank_account_updater;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hibernate based SessionFactory provider.
 */
public class DbSessionProvider {
    private final static Logger LOGGER = LoggerFactory.getLogger(DbSessionProvider.class);

    private static volatile DbSessionProvider instance;
    private SessionFactory sessionFactory;

    private DbSessionProvider(){
        try {
            setUp();
        } catch (Exception e) {
            LOGGER.warn("Was not able to initiate SessionFactory.", e);
        }
    }

    public static DbSessionProvider get() {
        DbSessionProvider localInstance = instance;
        if (localInstance == null) {
            synchronized (DbSessionProvider.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DbSessionProvider();
                }
            }
        }

        return localInstance;
    }

    public SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    /**
     * Baldly borrowed from https://docs.jboss.org/hibernate/orm/3.3/reference/en/html/tutorial.html
     */
    protected void setUp() throws Exception {
        // A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
    }
}
