package com.j2core.elozovan.week07.bank_account_updater.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Account model.
 */
@Entity
@Table (name = "accounts")
public class Account {
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    private long id;
    private String name;
    private double balance; //TODO: Think about BigDecimal.
    @Version
    @Column(name="version")
    private long version;

    public Account() {}

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Account{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", balance=").append(balance);
        sb.append(", version=").append(version);
        sb.append('}');
        return sb.toString();
    }
}
